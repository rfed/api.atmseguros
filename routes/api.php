<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace('Api')->group(function () {
    Route::prefix('v1')->group(function () {

        Route::namespace('Auth')->group(function () {
            Route::post('login', 'LoginController@login')->name('login');

            Route::post('logout', 'LoginController@logout')->name('logout');

        });

        Route::middleware(['auth:api', 'is.role:admin'])->prefix('admin')->group(function () {
            
            Route::prefix('motos')->group(function() {
                Route::namespace('Motorcycle')->group(function () {
                    Route::resource('brands', 'MotorcycleBrandController')->only(['index', 'show', 'destroy', 'update']);
                    Route::resource('models', 'MotorcycleModelController')->only(['index', 'show', 'destroy', 'update']);
                });

                Route::get('quotes', 'QuoteHeaderController@index');
                Route::get('solicitudes', 'SolicitudeController@index');
                Route::get('policies', 'PolicyController@index');

                Route::resource('parameters', 'ParameterController')->only(['index', 'show', 'destroy', 'update']);

            });
        });

        // todo middleware passport token
        // Route::middleware(['client_credentials'])->group(function () {

        Route::prefix('motos')->group(function() {
            Route::namespace('Motorcycle')->group(function () {
                Route::resource('brands', 'MotorcycleBrandController')->only(['index'])->name('index', 'motos.brands.index');
                Route::get('models-by-brand/{brand_id}', 'MotorcycleModelController@getByMotorcycleBrand')->name('motorcycle_model_by_brand');
                Route::get('value-by-model-year/{model_id}/{year}', 'MotorcycleYearController@getValueByMotorcycleModelAndYear')->name('motos.motorcycle_value_by_model_year');
            });
        });

        Route::prefix('autos')->group(function() {
            Route::namespace('Motorcycle')->group(function () {
                Route::resource('brands', 'MotorcycleBrandController')->only(['index'])->name('index', 'autos.brands.index');
                Route::get('models-by-brand-year/{brand_id}/{year}', 'MotorcycleModelController@getByMotorcycleBrandAndYear')->name('motorcycle_model_by_brand_year');
                Route::get('value-by-model-year/{model_id}/{year}', 'MotorcycleYearController@getValueByMotorcycleModelAndYear')->name('autos.motorcycle_value_by_model_year');
                Route::get('description-by-infoautocod/{infoautocod}', 'MotorcycleUsageController@getByMotorcycleInfoAutoCodCotiza')->name('description_by_infocodeauto');
            });
        });

        Route::resource('provinces', 'ProvinceController')->only(['index']);
        Route::get('cities-by-province/{province_id}', 'CityController@getByProvince')->name('city_by_province');
        Route::get('alarms', 'AlarmController@index')->name('alarms.index');
        Route::get('nacionalities', 'NacionalityController@index')->name('nacionalities.index');
        Route::get('inspection-centers', 'InspectionCenterController@index')->name('inspection-centers.index');
        Route::get('civil-status', 'CivilStatusController@index')->name('civil-status.index');

        Route::post('new-quote', 'QuoteHeaderController@newQuote')->name('new_quote');
        Route::post('hire-by-phone', 'SolicitudeController@store')->name('solicitude.store');
        Route::post('new-policy', 'PolicyController@store')->name('policy.store');
        Route::post('print-documents/{id}', 'PolicyController@printFiles')->name('policy.print');
        Route::post('validate-vehicle', 'PolicyController@validateVehicle')->name('policy.validate.vehicle');
        Route::get('parameters', 'ParameterController@index');
        Route::post('new-ticket', 'TicketController@store');

//      });

    });
});
