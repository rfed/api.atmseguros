<?php

use Illuminate\Database\Seeder;

class BenefitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $benefits = config('benefits');
        foreach ($benefits as $ben) {
            $b = \App\Entities\Api\Benefit::where('description', $ben)->first();
            if (is_null($b)) {
                $b = App\Entities\Api\Benefit::create([
                    'description'   => $ben['desc'],
                    'details'       => $ben['details']
                ]);
            }
        }

    }
}
