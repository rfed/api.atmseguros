<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_system = App\Entities\User\Role::where('key', \App\Entities\User\Role::getSystemKey())->first();
        if (is_null($role_system)) {
            $role_system = App\Entities\User\Role::create([
                'name' => "System",
                'key' => \App\Entities\User\Role::getSystemKey()
            ]);
        }

        $role_superadmin = App\Entities\User\Role::where('key', \App\Entities\User\Role::getSuperadminKey())->first();
        if (is_null($role_superadmin)) {
            $role_superadmin = App\Entities\User\Role::create([
                'name' => "Super Admin",
                'key' => \App\Entities\User\Role::getSuperadminKey()
            ]);
        }

        $role_admin = App\Entities\User\Role::where('key', \App\Entities\User\Role::getAdminKey())->first();
        if (is_null($role_admin)) {
            $role_admin = \App\Entities\User\Role::create([
                'name' => "Admin",
                'key' => \App\Entities\User\Role::getAdminKey()
            ]);
        }

        $role_user = App\Entities\User\Role::where('key', \App\Entities\User\Role::getUserKey())->first();
        if (is_null($role_user)) {
            $role_user = \App\Entities\User\Role::create([
                'name' => "User",
                'key' => \App\Entities\User\Role::getUserKey()
            ]);
        }

        // -- USERS

        $user_system = \App\Entities\User\User::where('email', 'system@atm.seguros.com')->first();
        if (empty($user_system)) {
            $user_system = App\Entities\user\User::create([
                'name' => 'system',
                'email' => 'system@atm.seguros.com',
                'password' => bcrypt('11235813'),
                'role_id' => $role_system->id,
                'email_verified_at' => \Carbon\Carbon::now()
            ]);
        }

        $user_superadmin = \App\Entities\User\User::where('email', 'superadmin@atm.seguros.com')->first();
        if (empty($user_superadmin)) {
            $user_superadmin = App\Entities\user\User::create([
                'name' => 'superadmin',
                'email' => 'superadmin@atm.seguros.com',
                'password' => bcrypt('11235813'),
                'role_id' => $role_superadmin->id,
                'email_verified_at' => \Carbon\Carbon::now()
            ]);
        }

        $admin_system = \App\Entities\User\User::where('email', 'admin@atm.seguros.com')->first();
        if (empty($admin_system)) {
            $admin_system = App\Entities\user\User::create([
                'name' => 'system',
                'email' => 'admin@atm.seguros.com',
                'password' => bcrypt('11235813'),
                'role_id' => $role_admin->id,
                'email_verified_at' => \Carbon\Carbon::now()
            ]);
        }

    }
}
