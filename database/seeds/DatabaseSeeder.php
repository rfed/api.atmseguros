<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(FtpCredentialsSeeder::class);
        $this->call(BenefitsSeeder::class);
        $this->call(CoveragesSeeder::class);
        $this->call(CoveragesBenefitSeeder::class);
        $this->call(ParametersSeeder::class);

    }
}
