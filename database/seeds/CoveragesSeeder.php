<?php

use Illuminate\Database\Seeder;

class CoveragesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coverages = config('coverages');

        foreach ($coverages as $cob) {
            $b = \App\Entities\Api\Coverage::where('code', $cob['code'])->first();
            if (is_null($b)) {
                $b = App\Entities\Api\Coverage::create($cob);
            }
        }
    }
}
