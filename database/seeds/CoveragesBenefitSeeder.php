<?php

use Illuminate\Database\Seeder;

class CoveragesBenefitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coverages = config('coverage-benefits');

        foreach ($coverages as $cob) {
            $c = \App\Entities\Api\Coverage::where('code', $cob['code'])->first();
            if (!is_null($c)) {
                foreach ($cob['benefits'] as $cov_ben) {
                    $ben_db = \App\Entities\Api\Benefit::where('description', $cov_ben['desc'])->first();
                    if (!is_null($ben_db)) {
                        $cov_b = \App\Entities\Api\CoverageBenefit::where('coverage_id', $c['id'])
                            ->where('benefit_id', $ben_db['id'])
                            ->first();

                        if (is_null($cov_b)) {
                            $b = App\Entities\Api\CoverageBenefit::create([
                                'coverage_id'   => $c->id,
                                'benefit_id'    => $ben_db->id,
                                'has_benefit'   => $cov_ben['active']
                            ]);
                        }

                    }
                }
            }
        }
    }
}
