<?php

use App\Entities\Parameter;
use Illuminate\Database\Seeder;

class ParametersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coverages = config('parameters');

        foreach ($coverages as $cob) {
            $b = Parameter::where('tag', $cob['tag'])->first();
            if (is_null($b)) {
                $b = Parameter::create($cob);
            }
        }
    }
}
