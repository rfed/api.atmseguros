<?php

use App\Entities\Api\Credentials\FtpCredential;
use App\Entities\Api\Credentials\SystemCredential;
use Illuminate\Database\Seeder;

class FtpCredentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ftp_call = FtpCredential::where('user', 'ftp_call')->first();
        if (is_null($ftp_call)) {
            $ftp_call = FtpCredential::create([
                'url'           => '200.110.135.88',
                'user'          => 'ftp_call',
                'password'      => '07CallATM2020%',
                'production'    => 0
            ]);
        }

        $u_call = SystemCredential::where('user', 'CALLCENTER')->first();
        if (is_null($u_call)) {
            $u_call = SystemCredential::create([
                'user'          => 'CALLCENTER',
                'password'      => '12CallATM34TEST',
                'production'    => 0,
                'url'           => 'http://extranet.atmseguros.com.ar/atmprueba/ws_autos.asmx?WSDL'
            ]);
        }

    }
}
