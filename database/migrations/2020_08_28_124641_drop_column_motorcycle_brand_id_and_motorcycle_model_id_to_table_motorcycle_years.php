<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnMotorcycleBrandIdAndMotorcycleModelIdToTableMotorcycleYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motorcycle_years', function (Blueprint $table) {
            $table->dropColumn('motorcycle_brand_id');
            $table->dropColumn('motorcycle_model_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motorcycle_years', function (Blueprint $table) {
            $table->bigInteger('motorcycle_brand_id')->unsigned();
            $table->bigInteger('motorcycle_model_id')->unsigned();

            $table->foreign('motorcycle_brand_id')
                ->references('id')
                ->on('motorcycle_brands');
            $table->foreign('motorcycle_model_id')
                ->references('id')
                ->on('motorcycle_models');
        });
    }
}
