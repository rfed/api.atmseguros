<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('policies', function (Blueprint $table) {
            $table->string('policy_number')->after('effective_date')->nullable();
            $table->string('proposal_number')->after('effective_date')->nullable();
            $table->string('status')->after('effective_date')->nullable();
        });
        Schema::table('policy_documents', function (Blueprint $table) {
            $table->text('request_json')->after('response')->nullable();
            $table->text('response_json')->after('request_json')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('policies', function (Blueprint $table) {
            $table->dropColumn('policy_number');
            $table->dropColumn('proposal_number');
            $table->dropColumn('status');
        });
        Schema::table('policy_documents', function (Blueprint $table) {
            $table->dropColumn('request_json');
            $table->dropColumn('response_json');
        });
    }
}
