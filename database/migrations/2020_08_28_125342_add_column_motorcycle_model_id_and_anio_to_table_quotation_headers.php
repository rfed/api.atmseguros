<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMotorcycleModelIdAndAnioToTableQuotationHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_headers', function (Blueprint $table) {
            $table->bigInteger('motorcycle_year_id')->unsigned()->nullable()->change();
            $table->bigInteger('motorcycle_model_id')->after('motorcycle_year_id')->unsigned()->nullable();
            $table->integer('year')->after('motorcycle_model_id');

            $table->foreign('motorcycle_model_id')
                ->references('id')
                ->on('motorcycle_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_headers', function (Blueprint $table) {
            $table->bigInteger('motorcycle_year_id')->unsigned()->nullable(false)->change();
            $table->dropForeign(['motorcycle_model_id']);
            $table->dropColumn('motorcycle_model_id');
            $table->dropColumn('year');
        });
    }
}
