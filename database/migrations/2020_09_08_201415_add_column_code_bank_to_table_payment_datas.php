<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCodeBankToTablePaymentDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_datas', function (Blueprint $table) {
            $table->tinyInteger('code_bank')->unsigned()->after('card_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_datas', function (Blueprint $table) {
            $table->dropColumn('code_bank');
        });
    }
}
