<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorcycleDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motorcycle_datas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('motorcycle_year_id')->unsigned();
            $table->foreign('motorcycle_year_id')->references('id')->on('motorcycle_years');

            $table->string('domain');
            $table->string('motor_number');
            $table->string('motor_chassis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motorcycle_datas');
    }
}
