<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');

            $table->bigInteger('motorcycle_year_id')->unsigned();
            $table->foreign('motorcycle_year_id')->references('id')->on('motorcycle_years');

            $table->boolean('has_alarm')->default(false);
            $table->integer('alarm_id')->unsigned()->nullable();
            $table->foreign('alarm_id')->references('id')->on('alarms');
            $table->boolean('cero_km')->default(false);
            $table->string('promotion_code')->nullable();
            $table->enum('usage', ['particular', 'professional'])->default('particular');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_headers');
    }
}
