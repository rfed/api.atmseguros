<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostalCodeQuoteHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_headers', function (Blueprint $table) {
            $table->string('postal_code')->nullable();
            $table->bigInteger('motorcycle_value')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_headers', function (Blueprint $table) {
            $table->dropColumn('postal_code');
        });
    }
}
