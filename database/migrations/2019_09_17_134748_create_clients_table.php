<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('document');
            $table->string('email');
            $table->string('name');
            $table->string('last_name');
            $table->integer('area_code');
            $table->integer('cellphone');
            $table->date('birthday');
            $table->integer('nacionality_id')->unsigned();
            $table->foreign('nacionality_id')->references('id')->on('nacionalities');
            $table->bigInteger('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->string('address_street');
            $table->integer('address_number');
            $table->integer('address_floor')->nullable();
            $table->string('address_apartment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
