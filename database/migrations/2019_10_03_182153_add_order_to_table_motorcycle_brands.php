<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToTableMotorcycleBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motorcycle_brands', function (Blueprint $table) {
            $table->integer('order')->after('is_suggested')->nullable();
        });
        Schema::table('motorcycle_models', function (Blueprint $table) {
            $table->integer('order')->after('is_suggested')->nullable();
        });
        Schema::table('provinces', function (Blueprint $table) {
            $table->integer('order')->after('description')->nullable();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->integer('order')->after('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_motorcycle_brands', function (Blueprint $table) {
            //
        });
    }
}
