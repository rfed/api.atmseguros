<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->bigIncrements('id');
            // quoation coverage
            $table->bigInteger('quotation_coverage_id')->unsigned();
            $table->foreign('quotation_coverage_id')->references('id')->on('quotation_coverages');

            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients');

            $table->bigInteger('motorcycle_data_id')->unsigned();
            $table->foreign('motorcycle_data_id')->references('id')->on('motorcycle_datas');

            $table->bigInteger('payment_data_id')->unsigned();
            $table->foreign('payment_data_id')->references('id')->on('payment_datas');

            $table->bigInteger('inspection_center_id')->unsigned();
            $table->foreign('inspection_center_id')->references('id')->on('inspection_centers');


            $table->date('effective_date');
            $table->text('request');
            $table->text('response');
            $table->boolean('error')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');
    }
}
