<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCoverages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coverages', function (Blueprint $table) {
            $table->text('details')->after('description')->nullable();
        });
        Schema::table('benefits', function (Blueprint $table) {
            $table->text('details')->after('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coverages', function (Blueprint $table) {
            $table->dropColumn('details');
        });
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropColumn('details');
        });
    }
}
