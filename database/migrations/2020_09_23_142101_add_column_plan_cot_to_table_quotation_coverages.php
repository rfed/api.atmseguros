<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPlanCotToTableQuotationCoverages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_coverages', function (Blueprint $table) {
            $table->char('plan_cot', 3)->nullable()->after('extra_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_coverages', function (Blueprint $table) {
            $table->dropColumn('plan_cot');
        });
    }
}
