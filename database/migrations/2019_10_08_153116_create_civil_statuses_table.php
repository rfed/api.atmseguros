<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCivilStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('civil_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('code');
            $table->timestamps();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('civil_status_id')->after('last_name')->unsigned();
            $table->foreign('civil_status_id')->references('id')->on('civil_statuses');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('civil_statuses');
    }
}
