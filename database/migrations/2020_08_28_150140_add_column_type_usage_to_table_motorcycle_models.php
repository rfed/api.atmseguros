<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTypeUsageToTableMotorcycleModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motorcycle_models', function (Blueprint $table) {
            $table->tinyInteger('type_usage')->unsigned()->after('usage_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motorcycle_models', function (Blueprint $table) {
            $table->dropColumn('type_usage');
        });
    }
}
