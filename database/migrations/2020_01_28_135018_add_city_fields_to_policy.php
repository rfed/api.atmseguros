<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityFieldsToPolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('policies', function (Blueprint $table) {
            $table->unsignedBigInteger('city_id')->nullable()
                ->default(null)
                ->after('client_id');

            $table->bigInteger('postal_code')->nullable()
                ->default(null)
                ->after('city_id');

            $table->unsignedBigInteger('province_id')->nullable()
                ->default(null)
                ->after('postal_code');

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('province_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('policies', function (Blueprint $table) {
//            $table->dropForeign(['city_id','province_id']);
            $table->dropForeign('policies_province_id_foreign');

            $table->dropForeign('policies_city_id_foreign');


            $table->dropColumn('province_id');
            $table->dropColumn('city_id');
            $table->dropColumn('postal_code');
        });
    }
}
