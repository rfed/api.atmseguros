<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorcycleYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motorcycle_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('motorcycle_brand_id')->unsigned();
            $table->foreign('motorcycle_brand_id')
                ->references('id')
                ->on('motorcycle_brands');
            $table->bigInteger('motorcycle_model_id')->unsigned();
            $table->foreign('motorcycle_model_id')
                ->references('id')
                ->on('motorcycle_models');

            $table->integer('year');
            $table->integer('value');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motorcycle_years');
    }
}
