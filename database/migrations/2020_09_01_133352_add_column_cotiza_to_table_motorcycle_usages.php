<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCotizaToTableMotorcycleUsages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motorcycle_usages', function (Blueprint $table) {
            $table->tinyInteger('cotiza')->after('description')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motorcycle_usages', function (Blueprint $table) {
            $table->dropColumn('cotiza');
        });
    }
}
