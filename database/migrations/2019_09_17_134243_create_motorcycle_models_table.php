<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorcycleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motorcycle_models', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('motorcycle_brand_id')->unsigned();
            $table->foreign('motorcycle_brand_id')
                    ->references('id')
                    ->on('motorcycle_brands');
            $table->string('code');
            $table->string('infoautocod');
            $table->string('description');
            $table->string('usage_code');
            $table->boolean('is_suggested')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motorcycle_models');
    }
}
