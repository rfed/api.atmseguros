<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_coverages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quotation_id')->unsigned();
            $table->foreign('quotation_id')->references('id')->on('quotations');
            $table->bigInteger('coverage_id')->unsigned();
            $table->foreign('coverage_id')->references('id')->on('coverages');
            $table->string('payment_method');
            $table->integer('fee_quantity');
            $table->float('fee_value');
            $table->float('prize');
            $table->text('extra_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_coverages');
    }
}
