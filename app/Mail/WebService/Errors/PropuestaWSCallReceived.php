<?php

namespace App\Mail\WebService\Errors;

use App\Entities\Api\Policy;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PropuestaWSCallReceived extends Mailable
{
    use Queueable, SerializesModels;

    protected $policy;
    protected $propuesta;
    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Policy $policy, $propuesta, $message)
    {
        $this->policy = $policy;
        $this->propuesta = $propuesta;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $brand = $this->policy->quotationCoverage->quotation->quoteHeader->motorcycleModel->motorcycleBrand;
        $model = $this->policy->quotationCoverage->quotation->quoteHeader->motorcycleModel;
        $quoteHeader =  $this->policy->quotationCoverage->quotation->quoteHeader;
        $client = $this->policy->client;

        $vehiculo = ( $brand->isAuto() ? 'AUTO' : 'MOTO' );

        if ( array_key_exists('tipoOperacion', $this->propuesta) && $this->propuesta['tipoOperacion'] == "PARCIAL" ) { 
            $subject = "ERROR EN EMISION PARCIAL DE ".$vehiculo;
        } else {
            $subject = "ERROR EN EMISION FINAL DE ".$vehiculo;
        }

        if ( empty($this->policy->postal_code) ) {
            $postal_code = $client->postal_code;
            $city_desc = $client->city->description;
        } else {
            $postal_code = $this->policy->postal_code;
            $city_desc = $this->policy->city->description;
        }

        return $this->subject($subject)
                ->markdown('emails.webService.errors.propuesta-wscall-received')
                ->with([
                    'quoteHeader'   => $quoteHeader,
                    'brand'         => $brand,
                    'model'         => $model,
                    'client'        => $client,
                    'postal_code'   => $postal_code,
                    'localidad'     => $city_desc,
                    'message'       => $this->message
                ]);
    }
}
