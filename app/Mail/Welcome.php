<?php

namespace App\Mail;

use App\Http\Resources\PolicyResource;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;
    protected $name;
    protected $policy;

    /**
     * Create a new message instance.
     *
     * @param $name
     * @param $documents
     */
    public function __construct($name, $policy)
    {
        $this->name = $name;
        $this->policy = $policy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $doc_name = [
            "POL" => 'poliza',
            "TAR" => 'tarjeta_de_circulacion',
            "MER" => 'certificado_mercosur',
            "CERT" => 'certificado_de_cobertura',
        ];
        $mail = $this->view('emails.welcome')
                    ->subject('Bienvenido a ATM Seguros')
                    ->with(['name' => $this->name, 'type' => $this->policy->status]);
        foreach ($this->policy->documents as $document) {
            $mail->attach($document['url'], [
                'as' => $doc_name[$document['type']]. '.pdf',
                'mime' => 'application/pdf'
            ]);
        }
        return $mail;
    }
}
