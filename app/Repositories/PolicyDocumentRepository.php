<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\PolicyDocument;
use App\Interfaces\Repositories\PolicyDocumentRepositoryInterface;
use Illuminate\Support\Facades\Storage;

class PolicyDocumentRepository extends BaseRepository implements PolicyDocumentRepositoryInterface
{
    /**
     * PolicyDocumentRepository constructor.
     *
     * @param PolicyDocument $policyDocument
     * @return void
     */
    public function __construct(PolicyDocument $policyDocument)
    {
        parent::__construct($policyDocument);
    }

    public function storeFromPrint($policy_id, $data, $doc) {

        $i =  $this->store([
            'request'               => $data['request'],
            'response'              => $data['response'],
            'request_json'          => $data['request_json'],
            'response_json'         => $data['response_json'],
            'error'                 => $data['error'],
            'type'                  => $doc,
            'policy_id'             => $policy_id,
            'url'                   => $data['url']
        ]);
        $file_req = 'Prints/Requests/' . $i->id . '.xml';
        $file_res = 'Prints/Responses/' . $i->id . '.xml';
        Storage::disk('public')->put($file_req, html_entity_decode($data['request']));
        Storage::disk('public')->put($file_res, html_entity_decode($data['response']));
        return $i;
    }
}
