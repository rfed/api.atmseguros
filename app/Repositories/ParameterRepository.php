<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Parameter;
use App\Interfaces\Repositories\ParameterRepositoryInterface;

class ParameterRepository extends BaseRepository implements ParameterRepositoryInterface
{
    /**
     * ParameterRepository constructor.
     *
     * @param Parameter $parameter
     * @return void
     */
    public function __construct(Parameter $parameter)
    {
        parent::__construct($parameter);
    }
}
