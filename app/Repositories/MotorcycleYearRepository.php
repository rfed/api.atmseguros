<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\MotorcycleYear;
use App\Interfaces\Repositories\MotorcycleYearRepositoryInterface;

class MotorcycleYearRepository extends BaseRepository implements MotorcycleYearRepositoryInterface
{
    /**
     * MotorcycleYearRepository constructor.
     *
     * @param MotorcycleYear $motorcycleYear
     * @return void
     */
    public function __construct(MotorcycleYear $motorcycleYear)
    {
        parent::__construct($motorcycleYear);
    }

    public function getValueByMotorcycleModelAndYear($model_id, $year) 
    {
        return $this->getModel()
                    ->newQuery()
                    ->where('year', $year)
                    ->whereHas('motorcycleModel', function($query) use($model_id) {
                        $query->where('id', $model_id);
                    })
                    ->pluck('value')
                    ->first();
    }

    public function getMinValueHigherByMotorcycleModelAndYear($model_id, $year)
    {
        return $this->getModel()
                    ->newQuery()
                    ->where('year', '>' ,$year)
                    ->whereHas('motorcycleModel', function($query) use($model_id) {
                        $query->where('id', $model_id);
                    })
                    ->min('value');
    }
}
