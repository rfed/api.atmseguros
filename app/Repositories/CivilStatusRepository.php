<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\CivilStatus;
use App\Interfaces\Repositories\CivilStatusRepositoryInterface;

class CivilStatusRepository extends BaseRepository implements CivilStatusRepositoryInterface
{
    /**
     * CivilStatusRepository constructor.
     *
     * @param CivilStatus $civilStatus
     * @return void
     */
    public function __construct(CivilStatus $civilStatus)
    {
        parent::__construct($civilStatus);
    }
}
