<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Quotation;
use App\Interfaces\Repositories\QuotationRepositoryInterface;

class QuotationRepository extends BaseRepository implements QuotationRepositoryInterface
{
    /**
     * QuotationRepository constructor.
     *
     * @param Quotation $quotation
     * @return void
     */
    public function __construct(Quotation $quotation)
    {
        parent::__construct($quotation);
    }
}
