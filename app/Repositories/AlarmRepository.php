<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Alarm;
use App\Interfaces\Repositories\AlarmRepositoryInterface;

class AlarmRepository extends BaseRepository implements AlarmRepositoryInterface
{
    /**
     * AlarmRepository constructor.
     *
     * @param Alarm $alarm
     * @return void
     */
    public function __construct(Alarm $alarm)
    {
        parent::__construct($alarm);
    }
}
