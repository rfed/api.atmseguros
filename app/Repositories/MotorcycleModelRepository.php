<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\MotorcycleModel;
use App\Interfaces\Repositories\MotorcycleModelRepositoryInterface;

class MotorcycleModelRepository extends BaseRepository implements MotorcycleModelRepositoryInterface
{
    /**
     * MotorcycleModelRepository constructor.
     *
     * @param MotorcycleModel $motorcycleModel
     * @return void
     */
    public function __construct(MotorcycleModel $motorcycleModel)
    {
        parent::__construct($motorcycleModel);
    }
}
