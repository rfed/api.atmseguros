<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\PaymentData;
use App\Interfaces\Repositories\PaymentDataRepositoryInterface;

class PaymentDataRepository extends BaseRepository implements PaymentDataRepositoryInterface
{
    /**
     * PaymentDataRepository constructor.
     *
     * @param PaymentData $paymentData
     * @return void
     */
    public function __construct(PaymentData $paymentData)
    {
        parent::__construct($paymentData);
    }
}
