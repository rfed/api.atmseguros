<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\City;
use App\Interfaces\Repositories\CityRepositoryInterface;

class CityRepository extends BaseRepository implements CityRepositoryInterface
{
    /**
     * CityRepository constructor.
     *
     * @param City $city
     * @return void
     */
    public function __construct(City $city)
    {
        parent::__construct($city);
    }
}
