<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\MotorcycleData;
use App\Interfaces\Repositories\MotorcycleDataRepositoryInterface;

class MotorcycleDataRepository extends BaseRepository implements MotorcycleDataRepositoryInterface
{
    /**
     * MotorcycleDataRepository constructor.
     *
     * @param MotorcycleData $motorcycleData
     * @return void
     */
    public function __construct(MotorcycleData $motorcycleData)
    {
        parent::__construct($motorcycleData);
    }
}
