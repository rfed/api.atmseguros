<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\MotorcycleBrand;
use App\Interfaces\Repositories\MotorcycleBrandRepositoryInterface;

class MotorcycleBrandRepository extends BaseRepository implements MotorcycleBrandRepositoryInterface
{
    /**
     * MotorcycleBrandRepository constructor.
     *
     * @param MotorcycleBrand $motorcycleBrand
     * @return void
     */
    public function __construct(MotorcycleBrand $motorcycleBrand)
    {
        parent::__construct($motorcycleBrand);
    }
}
