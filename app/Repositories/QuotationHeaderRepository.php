<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\QuotationHeader;
use App\Interfaces\Repositories\QuotationHeaderRepositoryInterface;

class QuotationHeaderRepository extends BaseRepository implements QuotationHeaderRepositoryInterface
{
    /**
     * QuotationHeaderRepository constructor.
     *
     * @param QuotationHeader $quotationHeader
     * @return void
     */
    public function __construct(QuotationHeader $quotationHeader)
    {
        parent::__construct($quotationHeader);
    }
}
