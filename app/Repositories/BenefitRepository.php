<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Benefit;
use App\Interfaces\Repositories\BenefitRepositoryInterface;

class BenefitRepository extends BaseRepository implements BenefitRepositoryInterface
{
    /**
     * BenefitRepository constructor.
     *
     * @param Benefit $benefit
     * @return void
     */
    public function __construct(Benefit $benefit)
    {
        parent::__construct($benefit);
    }
}
