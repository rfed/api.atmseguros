<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\InspectionCenter;
use App\Interfaces\Repositories\InspectionCenterRepositoryInterface;

class InspectionCenterRepository extends BaseRepository implements InspectionCenterRepositoryInterface
{
    /**
     * InspectionCenterRepository constructor.
     *
     * @param InspectionCenter $inspectionCenter
     * @return void
     */
    public function __construct(InspectionCenter $inspectionCenter)
    {
        parent::__construct($inspectionCenter);
    }
}
