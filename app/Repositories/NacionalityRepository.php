<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Nacionality;
use App\Interfaces\Repositories\NacionalityRepositoryInterface;

class NacionalityRepository extends BaseRepository implements NacionalityRepositoryInterface
{
    /**
     * NacionalityRepository constructor.
     *
     * @param Nacionality $nacionality
     * @return void
     */
    public function __construct(Nacionality $nacionality)
    {
        parent::__construct($nacionality);
    }
}
