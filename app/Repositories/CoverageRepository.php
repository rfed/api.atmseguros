<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Coverage;
use App\Interfaces\Repositories\CoverageRepositoryInterface;

class CoverageRepository extends BaseRepository implements CoverageRepositoryInterface
{
    /**
     * CoverageRepository constructor.
     *
     * @param Coverage $coverage
     * @return void
     */
    public function __construct(Coverage $coverage)
    {
        parent::__construct($coverage);
    }
}
