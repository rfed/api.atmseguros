<?php

namespace App\Repositories\WebService;

use Carbon\Carbon;
use App\Entities\Api\Policy;
use App\Entities\Api\QuoteHeader;
use App\Entities\Api\Credentials\SystemCredential;
use App\Entities\Parameter;

class WsQuoteRepository  
{
    /**
     * Generate body to quote
     * @param QuoteHeader $data
     * @param bool $value_zero
     * @param SystemCredential $user_system
     * @return array
     */
    public function newQuote(QuoteHeader $data, SystemCredential $user_system, $value_zero = false) 
    {
        $brand =  $data->motorcycleModel->motorcycleBrand;
        $model =  $data->motorcycleModel;
        $vendedor = Parameter::VendedorCotizacion();

        if (empty($data->city)) {
            $city = $data->province->code == 1 ? '1001': $data->postal_code;
        } else {
            $city = $data->city->code;
        }

        $suma = '0';
        if ($data->motorcycle_value > 0 & !$value_zero) {
            $suma = $data->motorcycle_value . ',00';
        }

        return [
            "usuario" => [
                "usa"       => $user_system->user,
                "pass"      => $user_system->password,
                "fecha"     => date('dmY'),
                //"vendedor"  => "0000100001",
                "vendedor" => $vendedor->value,
                "vigencia"  => "A",
                "origen"    => "PORTAL",
                "plan"      => 99
            ],
            "asegurado" => [
                "persona"   => "F",
                "iva"       => "CF",
                "infomotoclub" => "N",
                "cupondscto"=> empty($data->promotion_code) ? '' : $data->promotion_code
            ],
            "bien" => [
                "marca"     => $brand->code,
                "modelo"    => $model->code,
                "anofab"    => $data->year,
                "cerokm"    => $data->cero_km ? 'S' : 'N',
                "suma"      => $suma,
                "uso"       => $data->usage_code,
                "tipo_uso"  => $data->type_usage,
                "alarma"    => $data->has_alarm,
                "codpostal" => $city,
                'rastreo'   => $data->rastreo,
                "esventa"   => 0,
                "accesorios"   => [],
                'micrograbado' => 0,
                "seccion"   => $brand->section
            ]
        ];
    }

    public function newPolicy(Policy $policy, SystemCredential $user_system , $withPayment = true)
    {
        $birthday = Carbon::createFromFormat('Y-m-d', $policy->client->birthday)->format('dmY');
        $vendedor = Parameter::VendedorEmision();

        if (empty($policy->postal_code)) {
            $city = $policy->client->postal_code;
            $city_desc = $policy->client->city->description;
        } else {
            $city = $policy->postal_code;
            $city_desc = $policy->city->description;
        }
        
        $usuario = [
            "usa" => $user_system->user,
            "pass" => $user_system->password,
            //"vendedor" => $policy->inspection_center_code,
            "vendedor" => $vendedor->value,
            "origen" => "PORTAL",
        ];
        $propuesta = [
            "operacion" => $policy->quotationCoverage->quotation->operation_code,
            "cobertura" => $policy->quotationCoverage->coverage->code,
            "cond_pago" => "TARJETA",
            "ajuste" => empty($policy->quotationCoverage->adjustment_clause) ? '' : str_replace('%', '', $policy->quotationCoverage->adjustment_clause),
            "plan_cot" => $policy->quotationCoverage->plan_cot,
            "tipoOperacion" => "PARCIAL"
        ];
        $vehiculo = [
            "patente" => $policy->motorcycleData->domain,
            "chasis" => $policy->motorcycleData->motor_chassis,
            "motor" => $policy->motorcycleData->motor_number
        ];

        /*
         * si no es con pago (ver los parametros del metodo) se envia el pago vacio y la PROPUESTA con el tipo de operacion parcial
         * de lo contrario se envia el pago y el tipo de operacion parcial se remueve*/
        $payment = [];
        if ($withPayment) {
            $expires_in = Carbon::createFromFormat('Y-m-d', $policy->paymentData->expires_in)->format('mY');
            $payment = [
                "forma" => 2,
                "cuotacero" => '',
                "tarjeta" => [
                    "nombre" => $policy->paymentData->code_bank,
                    "numero" => $policy->paymentData->card_number,
                    "vcto" => $expires_in,
                    "titular_apellido" => $policy->paymentData->titular_last_name . ' ' . $policy->paymentData->titular_name,
                    "titular_tipodoc" => 'DNI',
                    "titular_nrodoc" => $policy->paymentData->titular_document,
                    "titular_provincia" => $policy->client->province->code,
                    "titular_localidad" => $city_desc,
                    "titular_calle" => $policy->client->address_street,
                    "titular_altura" => $policy->client->address_number,
                    "titular_piso" => $policy->client->address_floor,
                    "titular_depto" => $policy->client->address_apartment,
                    "titular_cp" => $city,
                ]
            ];
        }

        $titular = [
            "tipopersona" => "F",
            "tipodoc" => "DNI",
            "nrodoc" => $policy->client->document,
            "apellido" => $policy->client->last_name . ' ' . $policy->client->name,
            "provincia" => $policy->client->province->code,
            "localidad" => $city_desc,
            "calle" => $policy->client->address_street,
            "altura" => $policy->client->address_number,
            "piso" => $policy->client->address_floor,
            "depto" => $policy->client->address_apartment,
            "cp" => $city,
            "tel_part" => $policy->client->area_code . '-' . $policy->client->cellphone,
            "actividad" => (empty($policy->client->activity_id) ? 2 : $policy->client->activity_id),
            "mail" => $policy->client->email,
            "tel_cel" => $policy->client->area_code . '-' . $policy->client->cellphone,
            "sexo" => $policy->client->gender,
            "est_civil" => $policy->client->civilStatus->code,
            "nacionalidad" => $policy->client->nacionality->code,
            "cuit" => get_cuil_cuit($policy->client->document, $policy->client->gender),
            "prov_nac" => $policy->client->province->code,
            "loc_nac" => $city_desc,
            "fec_nac" => $birthday,
        ];
        
        $inspeccion = [
            "responsable" => "ASEGURADO",
            "combustible" => "Nafta",
            "color" => "",
            "km" => ""
        ];
        
        if ($policy->quotationCoverage->quotation->quoteHeader->cero_km) {
            $inspeccion = [
                "responsable" => "No Requiere Inspeccion",
                "combustible" => "Nafta",
                "color" => "",
                "km" => ""
            ];
        } else if ($policy->quotationCoverage->coverage->code === 'A0') {
            $inspeccion = [
                "responsable" => "No Requiere Inspeccion",
                "combustible" => "Nafta",
                "color" => "",
                "km" => ""
            ];
        };

        $WsQuoteDTO = [
            "usuario" => $usuario,
            "propuesta" => $propuesta,
            "titular" => $titular,
            "vehiculo" => $vehiculo,
            "formapago" => $payment,
            "inspeccion" => $inspeccion
        ];

        if ($withPayment) {
            unset($WsQuoteDTO['propuesta']['tipoOperacion']);
        }else{
            unset($WsQuoteDTO['formapago']);
//            $WsQuoteDTO['formapago'] = [];
        }

        return $WsQuoteDTO;
    }

    public function newPrint($operation_code, $informe, SystemCredential $user_system) 
    {
        return [
            "usa"       => $user_system->user,
            "pass"      => $user_system->password,
            "origen"    => "PORTAL",
            "operacion" => $operation_code,
            "informe"   => $informe
        ];
    }

    public function newPolicyPrint($pol_number, $informe, SystemCredential $user_system) 
    {
        $usuario = [
            "usa"       => $user_system->user,
            "pass"      => $user_system->password,
            "origen"    => "PORTAL",
        ];

        return [
            "usuario"   => $usuario,
            "pdf"       => [
                "seccion"   => 4,
                "poliza"    => $pol_number,
                "endoso"    => 0,
                "reporte"  => $informe
            ]
        ];
    }

    public function newValidateVehicle($data, SystemCredential $user_system) 
    {
        $usuario = [
            "usa"       => $user_system->user,
            "pass"      => $user_system->password,
            "origen"    => "PORTAL",
        ];

        return [
            "usuario"   => $usuario,
            "operacion" => [
                "fecha"     => $data['effective_date'],
                "patente"   => $data['domain'],
                "chasis"    => $data['chassis'],
                "dni"       => $data['document']
            ]
        ];
    }

}
