<?php

namespace App\Repositories\WebService;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Benefit;
use App\Interfaces\Repositories\BenefitRepositoryInterface;

class WsPolicyRepository extends BaseRepository implements BenefitRepositoryInterface
{
    /**
     * BenefitRepository constructor.
     *
     * @param Benefit $benefit
     * @return void
     */
    public function __construct(Benefit $benefit)
    {
        parent::__construct($benefit);
    }
}