<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\VatType;
use App\Interfaces\Repositories\VatTypeRepositoryInterface;

class VatTypeRepository extends BaseRepository implements VatTypeRepositoryInterface
{
    /**
     * VatTypeRepository constructor.
     *
     * @param VatType $vatType
     * @return void
     */
    public function __construct(VatType $vatType)
    {
        parent::__construct($vatType);
    }
}
