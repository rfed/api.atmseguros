<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Province;
use App\Interfaces\Repositories\ProvinceRepositoryInterface;

class ProvinceRepository extends BaseRepository implements ProvinceRepositoryInterface
{
    /**
     * ProvinceRepository constructor.
     *
     * @param Province $province
     * @return void
     */
    public function __construct(Province $province)
    {
        parent::__construct($province);
    }
}
