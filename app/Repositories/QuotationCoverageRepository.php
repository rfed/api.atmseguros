<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\QuotationCoverage;
use App\Interfaces\Repositories\QuotationCoverageRepositoryInterface;

class QuotationCoverageRepository extends BaseRepository implements QuotationCoverageRepositoryInterface
{
    /**
     * QuotationCoverageRepository constructor.
     *
     * @param QuotationCoverage $quotationCoverage
     * @return void
     */
    public function __construct(QuotationCoverage $quotationCoverage)
    {
        parent::__construct($quotationCoverage);
    }
}
