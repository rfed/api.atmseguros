<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\QuoteHeader;
use App\Interfaces\Repositories\QuoteHeaderRepositoryInterface;

class QuoteHeaderRepository extends BaseRepository implements QuoteHeaderRepositoryInterface
{
    /**
     * QuoteHeaderRepository constructor.
     *
     * @param QuoteHeader $quoteHeader
     * @return void
     */
    public function __construct(QuoteHeader $quoteHeader)
    {
        parent::__construct($quoteHeader);
    }
}
