<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Policy;
use App\Interfaces\Repositories\PolicyRepositoryInterface;

class PolicyRepository extends BaseRepository implements PolicyRepositoryInterface
{
    /**
     * PolicyRepository constructor.
     *
     * @param Policy $policy
     * @return void
     */
    public function __construct(Policy $policy)
    {
        parent::__construct($policy);
    }
}
