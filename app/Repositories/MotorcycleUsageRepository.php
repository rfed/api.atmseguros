<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\MotorcycleUsage;
use App\Interfaces\Repositories\MotorcycleUsageRepositoryInterface;

class MotorcycleUsageRepository extends BaseRepository implements MotorcycleUsageRepositoryInterface
{
    /**
     * MotorcycleUsageRepository constructor.
     *
     * @param MotorcycleUsage $motorcycleUsage
     * @return void
     */
    public function __construct(MotorcycleUsage $motorcycleUsage)
    {
        parent::__construct($motorcycleUsage);
    }
}
