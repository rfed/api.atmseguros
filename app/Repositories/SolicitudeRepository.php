<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Entities\Api\Solicitude;
use App\Interfaces\Repositories\SolicitudeRepositoryInterface;

class SolicitudeRepository extends BaseRepository implements SolicitudeRepositoryInterface
{
    /**
     * SolicitudeRepository constructor.
     *
     * @param Solicitude $solicitude
     * @return void
     */
    public function __construct(Solicitude $solicitude)
    {
        parent::__construct($solicitude);
    }
}
