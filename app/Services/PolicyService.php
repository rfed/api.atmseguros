<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\PolicyRepository;
use App\Interfaces\Services\PolicyServiceInterface;
use Illuminate\Support\Facades\Storage;

class PolicyService extends BaseService implements PolicyServiceInterface
{
    /**
     * PolicyService constructor.
     *
     * @param PolicyRepository $policyRepository
     * @return void
     */
    public function __construct(PolicyRepository $policyRepository)
    {
        parent::__construct($policyRepository);
    }

    public function updateFromPolicy($model, $data) {
        $file_req = 'Policies/Requests/' . $model->id . '.xml';
        $file_res = 'Policies/Responses/' . $model->id . '.xml';
        Storage::disk('public')->put($file_req, html_entity_decode($data['request']));
        Storage::disk('public')->put($file_res, html_entity_decode($data['response']));


        return $this->localRepository->update([
            'request'               => html_entity_decode($data['request']),
            'response'              => html_entity_decode($data['response']),
            'request_json'          => $data['request_json'],
            'response_json'         => $data['response_json'],
            'error'                 => $data['error'],
            'policy_number'         => $data['policy_number'],
            'proposal_number'       => $data['proposal_number'],
            'status'                => !$data['error'] ? $data['message'] : '',
            'message'               => $data['message']

        ], $model);
    }

    public function findOrCreate( $client , $motorcycle,  $payment, $data)
    {
        $policyData = [
            'client_id'             => $client->id,
            'motorcycle_data_id'    => $motorcycle->id,
            'city_id'               => $client->city->id,
            'province_id'           => $client->province->id,
            'postal_code'           => $client->city->code,
            'payment_data_id'       => (isset($payment) && $payment) ? $payment->id : null,
            'effective_date'        => $data['effective_date'],
            'quotation_coverage_id' => $data['quotation_coverage_id'],
            'pay_pending'           => (isset($payment) && $payment) ? 0 : 1,
        ];
        $policy = $this->localRepository->findByValues($policyData);

        if(empty($policy)){
            $policy = $this->store($policyData);
        }

        return $policy;

    }

}
