<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\CoverageRepository;
use App\Interfaces\Services\CoverageServiceInterface;

class CoverageService extends BaseService implements CoverageServiceInterface
{
    /**
     * CoverageService constructor.
     *
     * @param CoverageRepository $coverageRepository
     * @return void
     */
    public function __construct(CoverageRepository $coverageRepository)
    {
        parent::__construct($coverageRepository);
    }
    public function formatFromRequest(int $quotation_id, $coverages_request) {
        foreach ($coverages_request as $cov_req) {
            if (trim($cov_req['formapago']) === 'TARJETA') {
                $coverage_db = $this->localRepository->findByValues(['code' => $cov_req['codigo']]);
            }
        }
    }
}
