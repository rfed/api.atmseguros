<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\QuotationHeaderRepository;
use App\Interfaces\Services\QuotationHeaderServiceInterface;

class QuotationHeaderService extends BaseService implements QuotationHeaderServiceInterface
{
    /**
     * QuotationHeaderService constructor.
     *
     * @param QuotationHeaderRepository $quotationHeaderRepository
     * @return void
     */
    public function __construct(QuotationHeaderRepository $quotationHeaderRepository)
    {
        parent::__construct($quotationHeaderRepository);
    }
}
