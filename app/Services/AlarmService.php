<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\AlarmRepository;
use App\Interfaces\Services\AlarmServiceInterface;

class AlarmService extends BaseService implements AlarmServiceInterface
{
    /**
     * AlarmService constructor.
     *
     * @param AlarmRepository $alarmRepository
     * @return void
     */
    public function __construct(AlarmRepository $alarmRepository)
    {
        parent::__construct($alarmRepository);
    }
}
