<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\ProvinceRepository;
use App\Interfaces\Services\ProvinceServiceInterface;

class ProvinceService extends BaseService implements ProvinceServiceInterface
{
    /**
     * ProvinceService constructor.
     *
     * @param ProvinceRepository $provinceRepository
     * @return void
     */
    public function __construct(ProvinceRepository $provinceRepository)
    {
        parent::__construct($provinceRepository);
    }
}
