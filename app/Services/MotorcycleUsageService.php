<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\MotorcycleUsageRepository;
use App\Interfaces\Services\MotorcycleUsageServiceInterface;

class MotorcycleUsageService extends BaseService implements MotorcycleUsageServiceInterface
{
    /**
     * MotorcycleUsageService constructor.
     *
     * @param MotorcycleUsageRepository $motorcycleUsageRepository
     * @return void
     */
    public function __construct(MotorcycleUsageRepository $motorcycleUsageRepository)
    {
        parent::__construct($motorcycleUsageRepository);
    }

    public function getByMotorcycleInfoAutoCodCotiza($infoautocod)
    {
        return $this->localRepository->getModel()
                                    ->newQuery()
                                    ->where('cotiza', '=' ,1)
                                    ->whereHas('MotorcycleModels', function($query) use($infoautocod) {
                                        $query->where('infoautocod', $infoautocod);
                                    })
                                    ->get();
    }
}
