<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\ClientRepository;
use App\Interfaces\Services\ClientServiceInterface;

class ClientService extends BaseService implements ClientServiceInterface
{
    /**
     * ClientService constructor.
     *
     * @param ClientRepository $clientRepository
     * @return void
     */
    public function __construct(ClientRepository $clientRepository)
    {
        parent::__construct($clientRepository);
    }

    public function findOrCreate(array $data)
    {

        $client = $this->localRepository->findByValues(['document' => $data['document']]);

        if(!$client){
            $client = $this->localRepository->store($data);
        }else{
            $client = $this->localRepository->update($data, $client);

        };
        return $client;
    }

    public function findOrUpdate(array $data)
    {
        $client = $this->localRepository->findByValues(['document' => $data['document']]);

        if(!$client){
            $client = $this->localRepository->findByValues(["id" => $data["id"]]);
        }

        unset($data["id"]);
        return $this->localRepository->update($data, $client);
    }
}
