<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\NacionalityRepository;
use App\Interfaces\Services\NacionalityServiceInterface;

class NacionalityService extends BaseService implements NacionalityServiceInterface
{
    /**
     * NacionalityService constructor.
     *
     * @param NacionalityRepository $nacionalityRepository
     * @return void
     */
    public function __construct(NacionalityRepository $nacionalityRepository)
    {
        parent::__construct($nacionalityRepository);
    }
}
