<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\QuoteHeaderRepository;
use App\Interfaces\Services\QuoteHeaderServiceInterface;
use Illuminate\Database\Eloquent\Model;

class QuoteHeaderService extends BaseService implements QuoteHeaderServiceInterface
{
    /**
     * QuoteHeaderService constructor.
     *
     * @param QuoteHeaderRepository $quoteHeaderRepository
     * @return void
     */
    public function __construct(QuoteHeaderRepository $quoteHeaderRepository)
    {
        parent::__construct($quoteHeaderRepository);
    }
    public function store(array $data): Model
    {
        return parent::store($data);
    }
}
