<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\CoverageRepository;
use App\Repositories\QuotationCoverageRepository;
use App\Interfaces\Services\QuotationCoverageServiceInterface;
use Exception;

class QuotationCoverageService extends BaseService implements QuotationCoverageServiceInterface
{
    protected $coverageRepository;

    /**
     * QuotationCoverageService constructor.
     *
     * @param QuotationCoverageRepository $quotationCoverageRepository
     * @param CoverageRepository $coverageRepository
     */
    public function __construct(QuotationCoverageRepository $quotationCoverageRepository,
                                CoverageRepository $coverageRepository)
    {
        parent::__construct($quotationCoverageRepository);
        $this->coverageRepository = $coverageRepository;
    }

    public function storeFromQuote(int $quote_id, $coverages_request, $quote_header)
    {
        $seccion = $quote_header->motorcycleModel->motorcycleBrand->section;
        $quote_coverages = [];
        foreach ($coverages_request as $cov_req) {
            $coverage_db = $this->coverageRepository->findByValues([
                                                        'code'      => $cov_req['codigo'],
                                                        'seccion'   => $seccion,
                                                        'active'    => 1
                                                    ]);
            
            if( !empty($coverage_db) ) {
                $quote_coverages[] = $this->localRepository->store([
                        'quotation_id'          => $quote_id,
                        'coverage_id'           => $coverage_db->id,
                        'payment_method'        => trim($cov_req['formapago']),
                        'adjustment_clause'     => trim($cov_req['ajuste']),
                        'fee_quantity'          => $cov_req['cuotas'],
                        'fee_value'             => $cov_req['impcuotas'],
                        'prize'                 => $cov_req['premio'],
                        'plan_cot'              => $cov_req['plan_cot'],
                        'extra_data'            => $cov_req['ajuste']
                ]);
            }
        }
        
        if( count($quote_coverages) == 0 ) {
            throw new Exception('No existen coberturas disponibles.');
        }
        
        return $quote_coverages;
    }
}
