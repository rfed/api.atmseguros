<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\CivilStatusRepository;
use App\Interfaces\Services\CivilStatusServiceInterface;

class CivilStatusService extends BaseService implements CivilStatusServiceInterface
{
    /**
     * CivilStatusService constructor.
     *
     * @param CivilStatusRepository $civilStatusRepository
     * @return void
     */
    public function __construct(CivilStatusRepository $civilStatusRepository)
    {
        parent::__construct($civilStatusRepository);
    }
}
