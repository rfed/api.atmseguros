<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\QuotationRepository;
use App\Interfaces\Services\QuotationServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class QuotationService extends BaseService implements QuotationServiceInterface
{
    /**
     * QuotationService constructor.
     *
     * @param QuotationRepository $quotationRepository
     * @return void
     */
    public function __construct(QuotationRepository $quotationRepository)
    {
        parent::__construct($quotationRepository);
    }

    public function storeFromQuote(int $quote_header_id, $data): Model
    {
        $file_req = 'Quotation/Requests/' . $quote_header_id . '.xml';
        $file_res = 'Quotation/Responses/' . $quote_header_id . '.xml';
        Storage::disk('public')->put($file_req, html_entity_decode($data['request']));
        Storage::disk('public')->put($file_res, html_entity_decode($data['response']));
        
        $message = $data['message'];
        if( is_array($data['message']) ) {
            $message = implode(';', $data['message']);
        }

        return $this->localRepository->store([
            'quotation_header_id'   => $quote_header_id,
            'request'               => html_entity_decode($data['request']),
            'response'              => html_entity_decode($data['response']),
            'request_json'          => $data['request_json'],
            'response_json'         => $data['response_json'],
            'error'                 => $data['error'],
            'message'               => $message,
            'operation_code'        => $data['operation_code'],
            'value'                 => $data['value']
        ]);
    }
}
