<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\BenefitRepository;
use App\Interfaces\Services\BenefitServiceInterface;

class BenefitService extends BaseService implements BenefitServiceInterface
{
    /**
     * BenefitService constructor.
     *
     * @param BenefitRepository $benefitRepository
     * @return void
     */
    public function __construct(BenefitRepository $benefitRepository)
    {
        parent::__construct($benefitRepository);
    }
}
