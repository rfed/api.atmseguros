<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\VatTypeRepository;
use App\Interfaces\Services\VatTypeServiceInterface;

class VatTypeService extends BaseService implements VatTypeServiceInterface
{
    /**
     * VatTypeService constructor.
     *
     * @param VatTypeRepository $vatTypeRepository
     * @return void
     */
    public function __construct(VatTypeRepository $vatTypeRepository)
    {
        parent::__construct($vatTypeRepository);
    }
}
