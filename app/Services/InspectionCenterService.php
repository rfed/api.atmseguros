<?php

namespace App\Services;

use App\Core\Entities\CommonInternalResponse;
use App\Core\Services\BaseService;
use App\Repositories\InspectionCenterRepository;
use App\Interfaces\Services\InspectionCenterServiceInterface;
use Illuminate\Support\Facades\DB;

class InspectionCenterService extends BaseService implements InspectionCenterServiceInterface
{
    /**
     * InspectionCenterService constructor.
     *
     * @param InspectionCenterRepository $inspectionCenterRepository
     * @return void
     */
    public function __construct(InspectionCenterRepository $inspectionCenterRepository)
    {
        parent::__construct($inspectionCenterRepository);
    }

    public function getFromAtm() {
        $inspection_centers = $this->localRepository->getModel()->newQuery()
                                ->join('cotizador_localidad', 'cotizador_localidad.id', '=', 'centro.localidad_id')
                                ->select(['centro.*',
                                            'cotizador_localidad.codpos as postal_code',
                                            'cotizador_localidad.localidad as city_description',
                                            'cotizador_localidad.provincia as prov_description',
                                            'cotizador_localidad.codpro as prov_code'])

                                ->where('activado', 1)
                                ->orderBy('centro_nombre')
                                ->get();
        return $inspection_centers;
    }
}
