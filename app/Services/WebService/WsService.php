<?php

namespace App\Services\WebService;

use App\Entities\Api\Policy;
use App\Entities\Api\QuoteHeader;
use Spatie\ArrayToXml\ArrayToXml;
use Illuminate\Support\Facades\Storage;
use App\Core\Entities\CommonInternalResponse;
use App\Entities\Api\Credentials\FtpCredential;
use App\Entities\Api\Credentials\SystemCredential;
use App\Mail\WebService\Errors\PropuestaWSCallReceived;
use App\Repositories\WebService\WsPolicyRepository;
use App\Repositories\WebService\WsQuoteRepository;
use Illuminate\Support\Facades\Mail;

class WsService
{
    protected $wsQuoteRepository;
    protected $wsPolicyRepository;

    /**
     * MotorcycleUsageService constructor.
     *
     * @param WsQuoteRepository $wsQuoteRepository
     * @param WsPolicyRepository $wsPolicyRepository
     */
    public function __construct(WsQuoteRepository $wsQuoteRepository,
                                WsPolicyRepository $wsPolicyRepository)
    {
        $this->wsQuoteRepository = $wsQuoteRepository;
        $this->wsPolicyRepository = $wsPolicyRepository;
    }

    public function initNusoapClient($user_system) 
    {
        $client = new \nusoap_client($user_system->url, true, false, false,false, false, 9999999999);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;
        return $client;
    }

    public function getUserSystem() {
        return SystemCredential::where('production', isProduction())->orderBy('created_at', 'DESC')->first();
    }

    public function newQuote(QuoteHeader $header, $value_zero = false) 
    {
        // generate request of quote
        $user_system = $this->getUserSystem();
        $client = $this->initNusoapClient($user_system);

        $quote_body = $this->wsQuoteRepository->newQuote($header, $user_system, $value_zero);
        
        $err = $client->getError();
        if ($err) {
            return new CommonInternalResponse(true, 'Error al crear conexion con el ws');
        }
        
        // parse request to xml
        $arrayToXml =  ArrayToXml::convert($quote_body, 'auto');
        $arrayToXml = trim(str_replace('<?xml version="1.0"?>', '', $arrayToXml));
        
        // call de action
        $result = $client->call('AUTOS_Cotizar_PHP', array('doc_in' => $arrayToXml), '');

        $data = [   'request'       => $client->request,
                    'response'      => $client->response,
                    'request_json'  => json_encode($quote_body),
                    'response_json' => json_encode($result),
                    'quote'         => null,
                    'operation_code'=> null,
                    'value'         => 0,
                    'error'         => false,
                    'message'       => ''
        ];

        // parse response
        $err = $client->getError();
        if ($err) {
            $data['message'] = $err;
            $data['error'] = true;
            return $data;
        } else {
            $resp = $result['AUTOS_Cotizar_PHPResult']['auto'];
            if ($resp['statusSuccess'] === "FALSE") {
                $data['message'] = $this->processErrorResponse($resp);
                // SOLO COTIZA CON LA QUE SE MANDA
//                if ($data['message'] === 'Suma Asegurada fuera del desvío permitido') {
//                    return $this->newQuote($header, true);
//                }
                $data['error'] = true;
            } else {
                $data['operation_code'] = $resp['operacion'];
                $data['value'] = $resp['datos_cotiz']['suma'];
                $data['message'] = '';
                $data['data'] = $resp['cotizacion']['cobertura'];
            }
            return $data;
        }
    }

    public function newPolicy(Policy $policy) 
    {
        // generate request of quote
        $user_system = $this->getUserSystem();
        $client = $this->initNusoapClient($user_system);

        $withPayment = ! is_null($policy->paymentData);

        $quote_body = $this->wsQuoteRepository->newPolicy($policy, $user_system ,$withPayment);

        $err = $client->getError();
        if ($err) {
            return new CommonInternalResponse(true, 'Error al crear conexion con el ws');
        }

        // parse request to xml
        $arrayToXml =  ArrayToXml::convert($quote_body, 'auto_prop');
        $arrayToXml = trim(str_replace('<?xml version="1.0"?>', '', $arrayToXml));
        // call de action
        $result = $client->call('AUTOS_Propuesta_PHP', array('doc_in' => $arrayToXml), '');

        $data = [
            'request'           => $client->request,
            'response'          => $client->response,
            'request_json'      => json_encode($quote_body),
            'response_json'     => json_encode($result),
            'operation_code'    => null,
            'policy_number'     => 0,
            'proposal_number'   => 0,
            'error'             => false,
            'message'           => ''
        ];

        // parse response
        $err = $client->getError();
        if ($err) {
            $data['message'] = (array)$err;
            $data['error'] = true;
            // ENVIO EMAIL.
            $this->sendEmailErrorPropuestaWS($policy, $quote_body["propuesta"], $data['message']); 
            return $data;
        } else {
            $resp = $result['AUTOS_Propuesta_PHPResult']['auto_prop'];
            if ($resp['statusSuccess'] === "FALSE") {
                $data['message'] = (array)$this->processErrorResponse($resp);
                $data['error'] = true;
                // ENVIO EMAIL.
                $this->sendEmailErrorPropuestaWS($policy, $quote_body["propuesta"], $data['message']); 
            } else {
                $data['message'] = $resp['statusText'];
                // map response
                switch ($resp['statusText']) {
                    case "PRO":
                        $data['proposal_number'] = trim($resp['npropuesta']);
                        $data['policy_number'] = '0';
                        break;
                    case "POL":
                        $data['proposal_number'] = trim($resp['npropuesta']);
                        $data['policy_number'] = trim($resp['npoliza']);
                        break;
                    case "INS":
                        $data['proposal_number'] = trim($resp['nsolicitud']);
                        $data['policy_number'] = '0';
                        break;
                    default:
                        break;
                }
            }
            return $data;

        }
    }

    private function sendEmailErrorPropuestaWS($policy, $propuesta, $message)
    {
        return Mail::to($policy->client->email)->send(new PropuestaWSCallReceived($policy, $propuesta, $message));
    }

    public function dowloadDocuments(Policy $policy, $document) 
    {
        // generate request of quote
        $user_system = $this->getUserSystem();
        $client = $this->initNusoapClient($user_system);
        $quote_body = $this->wsQuoteRepository->newPrint($policy->quotationCoverage->quotation->operation_code, $document, $user_system);

        $err = $client->getError();
        if ($err) {
            return new CommonInternalResponse(true, 'Error al crear conexion con el ws');
        }
        // parse request to xml
        $arrayToXml =  ArrayToXml::convert($quote_body, 'auto_imp');
        $arrayToXml = trim(str_replace('<?xml version="1.0"?>', '', $arrayToXml));
        // call de action
        $result = $client->call('AUTOS_Impresion_PHP', array('doc_in' => $arrayToXml), '');

        $data = [
            'request'       => $client->request,
            'response'      => $client->response,
            'request_json'  => json_encode($quote_body),
            'response_json' => json_encode($result),
            'url'           => null,
            'error'         => false,
            'message'       => ''
        ];

        // parse response
        $err = $client->getError();
        if ($err) {
            $data['message'] = $err;
            $data['error'] = true;
            return $data;
        } else {
            $resp = $result['AUTOS_Impresion_PHPResult']['auto_imp'];
            if ($resp['statusSuccess'] === "FALSE") {
                $data['message'] = $this->processErrorResponse($resp);
                $data['error'] = true;
            } else {
                $data['url'] = $this->downloadFromFtp(0, 0, 0, $document, $policy->quotationCoverage->quotation->operation_code);

            }
            return $data;
        }
    }

    public function validateVehicle($data) 
    {
        // generate request of quote
        $user_system = $this->getUserSystem();
        $client = $this->initNusoapClient($user_system);
        $quote_body = $this->wsQuoteRepository->newValidateVehicle($data, $user_system);

        $err = $client->getError();
        if ($err) {
            return new CommonInternalResponse(true, 'Error al crear conexion con el ws');
        }
        // parse request to xml
        $arrayToXml =  ArrayToXml::convert($quote_body, 'auto_vigente');
        $arrayToXml = trim(str_replace('<?xml version="1.0"?>', '', $arrayToXml));
        // call de action
        $result = $client->call('AUTOS_VehiculoVigente', array('doc_in' => $arrayToXml), '');
        
        $data = [
            'request'       => $client->request,
            'response'      => $client->response,
            'request_json'  => json_encode($quote_body),
            'response_json' => json_encode($result),
            'url'           => null,
            'error'         => false,
            'message'       => ''
        ];
        
        // parse response
        $err = $client->getError();
        if ($err) {
            $data['message'] = $err;
            $data['error'] = true;
            return $data;
        } else {
            $resp = $result['AUTOS_VehiculoVigenteResult']['auto_vigente'];
            if ($resp['statusSuccess'] === "FALSE") {
                $data['message'] = $this->processErrorResponse($resp);
                $data['error'] = true;
            } else {
                if ($resp['statusText']['cod'] === '0') {  //if ($resp['statusText']['cod'] >= '0') {
                    $data['message'] = $resp['statusText']['msg'];
                    $data['error'] = false;
                } else {
                    $data['error'] = true;
                    if ($resp['asegurado'] != '' && $resp['vehiculo'] != '') {
                        $data['message'] = 'VEHICULO EXISTENTE';
                    } elseif ($resp['asegurado'] != '' && $resp['vehiculo'] === '') {
                        $data['message'] = 'ASEGURADO EXISTENTE';
                        $data['error'] = false;
                    } else {
                        $data['message'] = 'VEHICULO EXISTENTE';
                    }
                }
            }
            return $data;
        }
    }

    public function dowloadDocumentsPolicy(Policy $policy, $document) 
    {
        // generate request of quote
        $user_system = $this->getUserSystem();
        $client = $this->initNusoapClient($user_system);
        $quote_body = $this->wsQuoteRepository->newPolicyPrint($policy->policy_number, $document, $user_system);

        $err = $client->getError();
        if ($err) {
            return new CommonInternalResponse(true, 'Error al crear conexion con el ws');
        }
        // parse request to xml
        $arrayToXml =  ArrayToXml::convert($quote_body, 'auto_imp', true, 'UTF-8');
        $arrayToXml = trim(str_replace('<?xml version="1.0"?>', '', $arrayToXml));
        // call de action
        $result = $client->call('AUTOS_Reportes_PHP', array('doc_in' => $arrayToXml), '');

        $data = [
            'request'       => $client->request,
            'response'      => $client->response,
            'request_json'  => json_encode($quote_body),
            'response_json' => json_encode($result),
            'url'           => null,
            'error'         => false,
            'message'       => ''
        ];

        // parse response
        $err = $client->getError();
        if ($err) {
            $data['message'] = $err;
            $data['error'] = true;
            return $data;
        } else {
            $resp = $result['AUTOS_Reportes_PHPResult']['auto_imp'];
            if ($resp['statusSuccess'] === "FALSE") {
                $data['message'] = $this->processErrorResponse($resp);
                $data['error'] = true;
            } else {
                $data['url'] = $this->downloadFromFtp(4, $policy->policy_number, 0, $document, $policy->quotationCoverage->quotation->operation_code);
            }
            return $data;
        }
    }

    function downloadFromFtp($seccion, $poliza, $endoso, $document, $operation_code) {
//        if (Storage::disk('ftp_files')->exists($file))
//            Storage::disk('ftp_files')->move($file, $move_to);
        $ftp_cred = FtpCredential::where('production', isProduction())->first();

        config(['filesystems.disks.ftp' => [
            'driver' => 'ftp',
            'host'     => $ftp_cred->url,
            'username' => $ftp_cred->user,

            'password' => $ftp_cred->password,
            'port'     => '21',
            'timeout'  => '300'
        ]]);
        // download the new ones
        if ($poliza === 0) {
            $file_name = $document . '_' . $operation_code . '_WS.PDF';
        } else {
            $seccion = str_pad($seccion, 2, 0, STR_PAD_LEFT);
            $poliza = str_pad($poliza, 7, 0, STR_PAD_LEFT);
            $endoso = str_pad($endoso, 6, 0, STR_PAD_LEFT);
            $file_name = $document . '_' .$seccion . $poliza. $endoso . '_WS.PDF';
        }

        $file_to_download = 'Impresion/' . $file_name;
        $file_dest = '/' . $operation_code . '/'. $document . '/' . $file_name;
        if (Storage::disk('ftp')->exists($file_to_download)) {
            $exists = Storage::disk('documents')->exists($file_dest);
            if (!$exists) {
                $file_contents = Storage::disk('ftp')->get($file_to_download);
                $exists = Storage::disk('documents')->put($file_dest, $file_contents);
            }
            if ($exists) {
                return Storage::disk('documents')->url($file_dest);
            }
        }
        return false;
    }

    function getFakeDocuments(string $doc, string $status) {
        $data = [
            'request'       => '',
            'response'      => '',
            'request_json'  => '',
            'response_json' => '',
            'url'           => null,
            'error'         => false,
            'message'       => ''
        ];
        $policy_number = $status === 'POL' ? '4248552' : 0;
        $operation = '812430';

        $data['url'] = $this->downloadFromFtp(4, $policy_number, 0, $doc, $operation);
        return $data;
    }

    function getFakePolicy($policy) {
        $status = $policy->quotationCoverage->coverage->code === 'A0' ? 'POL' : 'PROP';
        $data = [
            'request'           => '',
            'response'          => '',
            'request_json'      => '',
            'response_json'     => '',
            'operation_code'    => null,
            'policy_number'     => "4248552",
            'proposal_number'   => "7812430",
            'error'             => false,
            'message'           => $status
        ];
        return $data;
    }

    function processErrorResponse($resp) {
        if (isset($resp['statusText']['msg'])) {
            return $resp['statusText']['msg'];
        } else {
            return $resp['statusText'];
        }
    }

}
