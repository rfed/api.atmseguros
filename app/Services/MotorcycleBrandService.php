<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\MotorcycleBrandRepository;
use App\Interfaces\Services\MotorcycleBrandServiceInterface;

class MotorcycleBrandService extends BaseService implements MotorcycleBrandServiceInterface
{
    /**
     * MotorcycleBrandService constructor.
     *
     * @param MotorcycleBrandRepository $motorcycleBrandRepository
     * @return void
     */
    public function __construct(MotorcycleBrandRepository $motorcycleBrandRepository)
    {
        parent::__construct($motorcycleBrandRepository);
    }
    
    public function getAll() 
    {
        return $this->checkIsAutoMoto()
                    ->newQuery()
                    ->whereHas('motorcycleModels')
                    ->orderBy('is_suggested', 'desc')
                    ->orderBy('description')
                    ->get();
    }

    private function checkIsAutoMoto()
    {
        $route = \Route::currentRouteName();
        
        if($route == 'autos.brands.index') {
            return $this->localRepository->getModel()->Auto();
        }else if($route == 'motos.brands.index') {
            return $this->localRepository->getModel()->Moto();
        }

        return $this->localRepository->getModel();
    }
}
