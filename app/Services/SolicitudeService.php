<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\SolicitudeRepository;
use App\Interfaces\Services\SolicitudeServiceInterface;

class SolicitudeService extends BaseService implements SolicitudeServiceInterface
{
    /**
     * SolicitudeService constructor.
     *
     * @param SolicitudeRepository $solicitudeRepository
     * @return void
     */
    public function __construct(SolicitudeRepository $solicitudeRepository)
    {
        parent::__construct($solicitudeRepository);
    }
}
