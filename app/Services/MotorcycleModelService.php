<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\MotorcycleModelRepository;
use App\Interfaces\Services\MotorcycleModelServiceInterface;

class MotorcycleModelService extends BaseService implements MotorcycleModelServiceInterface
{
    /**
     * MotorcycleModelService constructor.
     *
     * @param MotorcycleModelRepository $motorcycleModelRepository
     * @return void
     */
    public function __construct(MotorcycleModelRepository $motorcycleModelRepository)
    {
        parent::__construct($motorcycleModelRepository);
    }

    /**
     * Get motorcycle models by a motorcycle brand id
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByMotorcycleBrand($brand_id) 
    {
        return $this->localRepository->getModel()
                                    ->newQuery()
                                    ->where('motorcycle_brand_id', $brand_id)
                                    ->groupBy('description')
                                    ->orderBy('description')
                                    ->get();
    }

    public function getByMotorcycleBrandAndYear($brand_id, $year) 
    {
        return $this->localRepository->getModel()
                                    ->newQuery()
                                    ->where('motorcycle_brand_id', $brand_id)
                                    ->whereHas('MotorcycleYears', function($query) use($year) {
                                        $query->where('year', $year);
                                    })
                                    ->whereHas('MotorcycleUsage', function($query) {
                                        $query->where('cotiza', 1);
                                    })
                                    ->groupBy('description')
                                    ->orderBy('description')
                                    ->get(); 
    }
}
