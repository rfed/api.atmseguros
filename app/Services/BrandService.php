<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\BrandRepository;
use App\Interfaces\Services\BrandServiceInterface;

class BrandService extends BaseService implements BrandServiceInterface
{
    /**
     * BrandService constructor.
     *
     * @param BrandRepository $brandRepository
     * @return void
     */
    public function __construct(BrandRepository $brandRepository)
    {
        parent::__construct($brandRepository);
    }
}
