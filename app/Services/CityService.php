<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\CityRepository;
use App\Interfaces\Services\CityServiceInterface;

class CityService extends BaseService implements CityServiceInterface
{
    /**
     * CityService constructor.
     *
     * @param CityRepository $cityRepository
     * @return void
     */
    public function __construct(CityRepository $cityRepository)
    {
        parent::__construct($cityRepository);
    }

    public function getByProvince($province_id){
        if (!empty($this->localRepository)) {
            return $this->localRepository->getModel()
                ->newQuery()
                ->where('province_id', $province_id)
                ->orderBy('description')
                ->get();
        }
    }
}
