<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Entities\Api\City;
use App\Repositories\TicketRepository;

class TicketService extends BaseService
{
    /**
     * VatTypeService constructor.
     *
     * @param TicketRepository $ticketRepository
     * @return void
     */
    public function __construct(TicketRepository $ticketRepository)
    {
        parent::__construct($ticketRepository);
    }

    public function generateTicket($data)
    {        
        $city = City::where('id', $data['city_id'])->first();
        $submotivo = '';
        switch ($data['reason']) {
            case 'policy_error':
                $submotivo = 105;
                break;
            case 'payment_error':
                $submotivo = 104;
                break;
        }

        $resp = [
            "socio_id"          => 0,
            "Propuesta"         => 0,
            "MotivoID"          => 24,
            "SubmotivoID"       => $submotivo,
            "Comentario"        => $data['comment'] ?? null,
            "TicketEstadoID"    => 7,
            "UsuarioID"         => 0,
            "AgenciaID"         => 1,
            "Sitio"             => "A",
            "Nombre"            => $data['name'],
            "Apellido"          => $data['last_name'],
            "Provincia"         => $city->province->description ?? null,
            "Localidad"         => $city->description ?? null,
            "TipoDocumentoID"   => "DNI",
            "DocNumero"         => $data['document'] ?? null,
            "TelPartArea"       => "",
            "TelPart"           => "",
            "TelCelArea"        => $data['area_code'] ?? null,
            "TelCel"            => $data['cell_phone'] ?? null,
            "Email"             => $data['email'] ?? null,
            "EmailNotificacion" => $data['email'] ?? null,
            "Imagenes"          => "",
            "UsuarioStr"        => "",
            "AgenciaStr"        => "",
            "OrganizadorStr"    => "",
            "OrganizadorID"     => "",
            "TipoTarjetaStr"    => "",
            "Efectivo"          => "",
            "referencia_id"     => "",
            "u"                 => "dibi",
            "p"                 => "6D6D5F54881D9B8304BF47C0127BAEB2"
        ];

        $ch = curl_init();
        $resp = http_build_query($resp);
        curl_setopt($ch, CURLOPT_URL,"http://backoffice.atmseguros.com.ar/atmdev/eCompania/AtmSys.WS/api_rest.php/Consultas");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $resp);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = json_decode(curl_exec($ch), true);        
        curl_close ($ch);

        if (!$server_output['error'] && !$server_output['warning'] ) {
            $data = [
                "code"  => $server_output['result'],
                "data"  => json_encode($data),
                "description" => 'OK'
            ];
        } else {
            $data = [
                "code"  => "error",
                "data"  => json_encode($data),
                "description" => json_encode($server_output['result'])
            ];
        }
        $this->store($data);
    }
}
