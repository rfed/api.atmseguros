<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Entities\Api\Policy;
use App\Repositories\PolicyDocumentRepository;
use App\Interfaces\Services\PolicyDocumentServiceInterface;
use App\Services\WebService\WsService;

class PolicyDocumentService extends BaseService implements PolicyDocumentServiceInterface
{
    protected $wsService;

    /**
     * PolicyDocumentService constructor.
     *
     * @param PolicyDocumentRepository $policyDocumentRepository
     * @param WsService $wsService
     */
    public function __construct(PolicyDocumentRepository $policyDocumentRepository,
                                WsService $wsService)
    {
        parent::__construct($policyDocumentRepository);
        $this->localRepository = $policyDocumentRepository;
        $this->wsService = $wsService;
    }

    public function printDocuments(Policy $policy) {
        $imp = [];
        $documents = $policy->status === 'POL' ? ['POL', 'MER', 'TAR'] : ['SOL'];
        foreach ($documents as $doc) {
            $document = $this->localRepository->getModel()
                ->newQuery()
                ->where('policy_id', $policy->id)
                ->where('type', $doc)
                ->whereNotNull('url')
                ->first();
            if (empty($document)) {

//                if (isProduction()) {
                    if ($doc === "SOL") {
                        $print_response = $this->wsService->dowloadDocuments($policy, $doc);
                    } else {
                        $print_response = $this->wsService->dowloadDocumentsPolicy($policy, $doc);
                    }
//                } else {
//                    $print_response = $this->wsService->getFakeDocuments($doc, $policy->status);
//                }
                $document = $this->localRepository->storeFromPrint($policy->id, $print_response, $doc);
            }
            $imp[] = $document;
        }
        return $imp;
    }
}
