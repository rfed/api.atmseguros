<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\MotorcycleDataRepository;
use App\Interfaces\Services\MotorcycleDataServiceInterface;

class MotorcycleDataService extends BaseService implements MotorcycleDataServiceInterface
{
    /**
     * MotorcycleDataService constructor.
     *
     * @param MotorcycleDataRepository $motorcycleDataRepository
     * @return void
     */
    public function __construct(MotorcycleDataRepository $motorcycleDataRepository)
    {
        parent::__construct($motorcycleDataRepository);
    }

    public function findOrCreate(array $data)
    {
        $motorcycle = $this->localRepository->findByValues(['domain' => $data['domain']]);
        if(!$motorcycle){
            $motorcycle = $this->localRepository->store($data);
        };
        return $motorcycle;
    }
}
