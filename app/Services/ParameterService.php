<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\ParameterRepository;
use App\Interfaces\Services\ParameterServiceInterface;

class ParameterService extends BaseService implements ParameterServiceInterface
{
    /**
     * ParameterService constructor.
     *
     * @param ParameterRepository $parameterRepository
     * @return void
     */
    public function __construct(ParameterRepository $parameterRepository)
    {
        parent::__construct($parameterRepository);
    }
}
