<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\MotorcycleYearRepository;
use App\Interfaces\Services\MotorcycleYearServiceInterface;

class MotorcycleYearService extends BaseService implements MotorcycleYearServiceInterface
{
    /**
     * MotorcycleYearService constructor.
     *
     * @param MotorcycleYearRepository $motorcycleYearRepository
     * @return void
     */
    public function __construct(MotorcycleYearRepository $motorcycleYearRepository)
    {
        parent::__construct($motorcycleYearRepository);
    }
    /**
     * Get motorcycle years by a motorcycle brand id and model id
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getValueByMotorcycleModelAndYear($model_id, $year) 
    {
        if (!empty($this->localRepository)) {

            $value = $this->localRepository->getValueByMotorcycleModelAndYear($model_id, $year);

            if( $value ) {
                return $value;
            }
            
            if( $this->esMoto() ) {
                
                $min_value_years_higher = $this->localRepository->getMinValueHigherByMotorcycleModelAndYear($model_id, $year);
    
                if ( $min_value_years_higher ) {
                    return $min_value_years_higher;
                }
    
                return 0;
            }
        }
    }

    private function esMoto()
    {   
        if(\Route::currentRouteName() == 'motos.motorcycle_value_by_model_year') {
            return true;
        }
        return false;
    }

}
