<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Repositories\PaymentDataRepository;
use App\Interfaces\Services\PaymentDataServiceInterface;

class PaymentDataService extends BaseService implements PaymentDataServiceInterface
{
    /**
     * PaymentDataService constructor.
     *
     * @param PaymentDataRepository $paymentDataRepository
     * @return void
     */
    public function __construct(PaymentDataRepository $paymentDataRepository)
    {
        parent::__construct($paymentDataRepository);
    }
}
