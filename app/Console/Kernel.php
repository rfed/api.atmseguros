<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Imports\CitiesImport::class,
        \App\Console\Commands\Imports\MotorcycleImport::class,
        \App\Console\Commands\Imports\MotorcycleUsageImport::class,
        \App\Console\Commands\Imports\DownloadFtpFiles::class,
        \App\Console\Commands\Imports\NacionalitiesImport::class,
        \App\Console\Commands\Imports\VatImport::class,
        \App\Console\Commands\Imports\ImportData::class,
        \App\Console\Commands\Imports\CivilStatusImport::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
