<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\CivilStatus;
use App\Entities\Api\Nacionality;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CivilStatusImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:civil-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'nacionalities import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Importacion WS_AU_EST_CIVIL - Comienzo: ' . date('m-d-Y H:i:s'));

        $url = public_path('ftp_files/WS_AU_EST_CIVIL');
        $archivo = array_filter(explode(PHP_EOL,file_get_contents($url)));

        //Saco la primer linea porque tiene las cabeceras de la tabla
        unset($archivo[0]);
        // codpos;localidad;provincia;codpro
        $quant_lines = count($archivo);
        $ac = 0;

        foreach ($archivo as $key => $linea) {
            $valores = explode(";", $linea);
            $nac = CivilStatus::where('code', $valores[0])->first();

            if (empty($nac)) {
                $nac = new CivilStatus(['code' => $valores[0], 'description' => trim($valores[1])]);
                $nac->save();
                $ac++;
            }
        }
        Log::info('IMPORT-CIVIL-STATUS - Lines: ' . $quant_lines . ' | Civil Status: ' . $ac );
        Log::debug('Importacion WS_AU_EST_CIVIL - Fin: ' . date('m-d-Y H:i:s'));

        return true;

    }
}
