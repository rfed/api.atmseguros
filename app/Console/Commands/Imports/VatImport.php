<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\Nacionality;
use App\Entities\Api\VatType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class VatImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:vat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'vat import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Importacion WS_AU_IVA - Comienzo: ' . date('m-d-Y H:i:s'));

        $url = public_path('ftp_files/WS_AU_IVA');
        $archivo = array_filter(explode(PHP_EOL,file_get_contents($url)));

        //Saco la primer linea porque tiene las cabeceras de la tabla
        unset($archivo[0]);
        // codpos;localidad;provincia;codpro
        $quant_lines = count($archivo);
        $ac = 0;

        foreach ($archivo as $key => $linea) {
            $valores = explode(";", $linea);
            $nac = VatType::where('code', $valores[0])->first();

            if (empty($nac)) {
                $nac = new VatType(['code' => $valores[0], 'description' => $valores[1]]);
                $nac->save();
                $ac++;
            }
        }
        Log::info('IMPORT-vat - Lines: ' . $quant_lines . ' | VAT: ' . $ac );
        Log::debug('Importacion WS_AU_IVA - Fin: ' . date('m-d-Y H:i:s'));
        return true;

    }
}
