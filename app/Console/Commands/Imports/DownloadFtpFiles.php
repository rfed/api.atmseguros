<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\Credentials\FtpCredential;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DownloadFtpFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:ftp_files';
    protected $files = [
        'WS_AU_INFOAUTO',
        'WS_AU_LOCALIDADES',
        'wS_AU_MARCA_MODELO', 'WS_AU_USOS',
        'WS_AU_NACIONALIDAD', 'WS_AU_IVA',
        'WS_AU_EST_CIVIL'
    ];

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Descarga los archivos ftp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ftp_cred = FtpCredential::where('production',  isProduction())->first();

        config(['filesystems.disks.ftp' => [
            'driver' => 'ftp',
            'host'     => $ftp_cred->url,
            'username' => $ftp_cred->user,
            'password' => $ftp_cred->password,
            'port'     => '21',
            'timeout'  => '300'
        ]]);

        foreach ($this->files as $file) {
            // move to old files
            $move_to = 'old/' . date('d-m-Y') . '/' . $file;
            if (Storage::disk('ftp_files')->exists($file))
                Storage::disk('ftp_files')->move($file, $move_to);
            // download the new ones
            $file_to_download = 'Parametros/' . $file;
            $file_contents = Storage::disk('ftp')->get($file_to_download);
            Storage::disk('ftp_files')->put($file, utf8_encode($file_contents));
        }

        return true;
    }
}
