<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\MotorcycleUsage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MotorcycleUsageImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:motorcycle-usages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Importacion WS_AU_USOS - Comienzo: ' . date('m-d-Y H:i:s'));

        $url = public_path('ftp_files/WS_AU_USOS');
        $archivo = array_filter(explode(PHP_EOL,file_get_contents($url)));

        //Saco la primer linea porque tiene las cabeceras de la tabla
        unset($archivo[0]);
        // codpos;localidad;provincia;codpro
        $quant_lines = count($archivo);
        $usages = 0;

        foreach ($archivo as $key => $linea) {
            $valores = explode(";", $linea);
            $usage = MotorcycleUsage::where('code', $valores[0])->first();

            if (empty($usage)) {
                $usage = new MotorcycleUsage(['code' => $valores[0], 'description' => $valores[1]]);
                $usage->save();
                $usages++;
            }
        }
        Log::info('IMPORT-USAGES - Lines: ' . $quant_lines . ' | Usages: ' . $usages );
        Log::debug('Importacion WS_AU_USOS - Fin: ' . date('m-d-Y H:i:s'));
        return true;
    }
}
