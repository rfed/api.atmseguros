<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\Nacionality;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class NacionalitiesImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:nacionalities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'nacionalities import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Importacion WS_AU_NACIONALIDAD - Comienzo: ' . date('m-d-Y H:i:s'));

        $url = public_path('ftp_files/WS_AU_NACIONALIDAD');
        $archivo = array_filter(explode(PHP_EOL,file_get_contents($url)));

        //Saco la primer linea porque tiene las cabeceras de la tabla
        unset($archivo[0]);
        // codpos;localidad;provincia;codpro
        $quant_lines = count($archivo);
        $ac = 0;

        foreach ($archivo as $key => $linea) {
            $valores = explode(";", $linea);
            $nac = Nacionality::where('code', $valores[0])->first();

            if (empty($nac)) {
                $nac = new Nacionality(['code' => $valores[0], 'description' => $valores[1]]);
                $nac->save();
                $ac++;
            }
        }
        Log::info('IMPORT-NAC - Lines: ' . $quant_lines . ' | Nacionalities: ' . $ac );
        Log::debug('Importacion WS_AU_NACIONALIDAD - Fin: ' . date('m-d-Y H:i:s'));

        return true;

    }
}
