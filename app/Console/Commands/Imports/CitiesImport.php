<?php

namespace App\Console\Commands\Imports;

use App\Entities\Api\City;
use App\Entities\Api\Province;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CitiesImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa las ciudades desde archivos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::disableQueryLog();
        Log::debug('Importacion WS_AU_LOCALIDADES - Comienzo: ' . date('m-d-Y H:i:s'));

        try {
            $url = public_path('ftp_files/WS_AU_LOCALIDADES');
            $archivo = array_filter(explode(PHP_EOL,file_get_contents($url)));

            //Saco la primer linea porque tiene las cabeceras de la tabla
            unset($archivo[0]);
            // codpos;localidad;provincia;codpro
            $quant_lines = count($archivo);
            $provinces_up = 0;
            $cities_up = 0;

            foreach ($archivo as $key => $linea) {
                $valores = explode(";", $linea);
                $province = Province::where('code', $valores[3])->first();

                if (empty($province)) {
                    $province = new Province(['code' => $valores[3], 'description' => $valores[2]]);
                    $province->save();
                    $provinces_up++;
                }
                $valores[1] = str_replace('�', '',$valores[1] );
                $city = City::where('code', $valores[0])
                            ->where('description', $valores[1])
                            ->first();

                if (empty($city)) {
                    $city = new City([  'code'          => $valores[0],
                                        'description'   => $valores[1],
                                        'province_id'   => $province->id]);
                    $city->save();
                    $cities_up++;
                }
            }
            Log::info('IMPORT-CITIES - Lines: ' . $quant_lines . ' | Provincies: ' . $provinces_up . ' | Cities: ' . $cities_up);

        }catch (\Exception $exception) {
            Log::error('Error WS_AU_LOCALIDADES - ERROR: ' . $exception->getMessage());
        }
        Log::debug('Importacion WS_AU_LOCALIDADES - Fin: ' . date('m-d-Y H:i:s'));
        return true;

    }
}
