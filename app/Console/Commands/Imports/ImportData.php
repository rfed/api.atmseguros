<?php

namespace App\Console\Commands\Imports;

use Illuminate\Console\Command;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import  data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(10000);
        ini_set('memory_limit', '-1');

        $this->call('download:ftp_files');
        $this->info('Archivos FTP descargados');

        $this->call('import:cities');
        $this->info('Localidades descargadas');

        $this->call('import:motorcycle-usages');
        $this->info('Usos Motos descargadas');

        $this->call('import:nacionalities');
        $this->info('Nacionalidades descargadas');

        $this->call('import:vat');
        $this->info('Iva descargadas');


    }
}
