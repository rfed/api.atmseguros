<?php

namespace App\Console\Commands\Imports;


use App\Http\Controllers\Api\InfoautoController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MotorcycleImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:motorcycles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa las marcas, modelos y anios desde archivos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::disableQueryLog();
        Log::debug('Importacion INFOAUTO - Comienzo marcas y modelos: ' . date('m-d-Y H:i:s'));

        $marcas = App::make(InfoautoController::class)->importBrandsAndModels();
        Log::debug('Importacion INFOAUTO - FIN marcas y modelos: ' . date('m-d-Y H:i:s'));

        Log::debug('Importacion INFOAUTO - Anios y sumas: ' . date('m-d-Y H:i:s'));
        $values = App::make(InfoautoController::class)->importValues();
        Log::debug('Importacion INFOAUTO - FIN Anios y sumas: ' . date('m-d-Y H:i:s'));
        return true;
    }
}
