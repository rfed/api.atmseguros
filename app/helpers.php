<?php
if (!function_exists('getUser')) {
    /**
     * Return current user
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function getUser()
    {
        return auth('api')->user();
    }
}

if (!function_exists('isLogged')) {
    /**
     * Return boolean if the someone is logged
     * @return boolean
     */
    function isLogged()
    {
        return (auth('api')->user() != null);
    }
}

if (!function_exists('parseTrueFalseNull')) {
    /**
     * Return boolean if the someone is logged
     * @return boolean
     */
    function parseTrueFalseNull($value)
    {
        if (is_null($value)) {
            return null;
        }
        switch ($value) {
            case 1:
                return true;
            case 0:
                return false;
            default:
                return null;
        }
    }
}
function parseCsvToArray($path, $delimiter = ';') {
    $data = [];
    $header = null;
    if (($handle = fopen($path, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }
    return $data;
}

function get_cuil_cuit($document_number, $gender){
    /**
     * Verifico que el document_number tenga exactamente ocho numeros y que
     * la cadena no contenga letras.
     */
    if(strlen($document_number) != 8 || is_numeric($document_number)) {
        if (strlen($document_number) == 7 && !is_numeric($document_number)) {
            $document_number = '0' . $document_number;
        }
    }

    /**
     * De esta manera permitimos que el gender venga en minusculas,
     * mayusculas y titulo.
     */

    // Defino el valor del prefijo.
    switch ($gender) {
        case 'M':
            $AB = '20';
            break;
        case 'F':
            $AB = '27';
            break;
        default:
            $AB = '20';
            break;
    }

    /*
     * Los numeros (excepto los dos primeros) que le tengo que
     * multiplicar a la cadena formada por el prefijo y por el
     * numero de $document_number los tengo almacenados en un arreglo.
     */
    $multiplicadores = [3, 2, 7, 6, 5, 4, 3, 2];

    // Realizo las dos primeras multiplicaciones por separado.
    $calculo = (((int)($AB[0]) * 5) + ((int)($AB[1]) * 4));

    /*
     * Recorro el arreglo y el numero de $document_number para
     * realizar las multiplicaciones.
     */
    for ($i = 0; $i< strlen($document_number); $i++) {
        $calculo += (int)($document_number[$i] * $multiplicadores[$i]);
    }

    // Calculo el resto.
    $resto = ((int)($calculo)) % 11;

    /*
     * Llevo a cabo la evaluacion de las tres condiciones para
     * determinar el valor de C y conocer el valor definitivo de
     * $AB.
     */
    if($resto==1){
        if($gender === 'M'){
            $C = '9';
        } else {
            $C = '4';
        }
        $AB = '23';
    } else if($resto === 0){
        $C = '0';
    } else {
        $C = 11 - $resto;
    }

    // Generate cuit
    $cuil_cuit = $AB . $document_number. $C;

    return $cuil_cuit;
}
function isProduction() {
    return config('env') === 'production' ? '1' : '0';
}