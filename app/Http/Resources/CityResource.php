<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'description'   => $this->description,
            'code'          => $this->code,
            'order'         => $this->order,
            'deletedAt'     => empty($this->deleted_at) ? null : $this->deleted_at->format('d-m-Y H:i')
        ];    }
}
