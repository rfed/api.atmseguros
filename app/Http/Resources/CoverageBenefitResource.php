<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CoverageBenefitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->benefit->id,
            'description'   => $this->benefit->description,
            'details'       => $this->benefit->details,
            'hasBenefit'    => $this->has_benefit
        ];
    }
}
