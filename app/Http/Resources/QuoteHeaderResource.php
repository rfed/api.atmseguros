<?php

namespace App\Http\Resources;

use App\Http\Resources\CityResource;
use App\Http\Resources\ProvinceResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\QuotationCoverageResource;
use App\Http\Resources\Motorcycle\MotorcycleYearResource;
use App\Http\Resources\Motorcycle\MotorcycleBrandResource;
use App\Http\Resources\Motorcycle\MotorcycleModelResource;

class QuoteHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $motorcycleYear = $this->motorcycleModel->motorcycleYears->filter(function ($value, $key) {
            return $value->year == $this->year;
        })->first();

        return [
            'id'                => $this->id,
            'operationCode'     => $this->quotation->operation_code,
            'city'              => !empty($this->city) ? new CityResource($this->city) : null,
            'postalCode'        => !empty($this->postal_code) ? $this->postal_code : '',
            'year'              => $this->year,
            'client'            => new ClientResource($this->client),
            'province'          => new ProvinceResource($this->province),
            'motorcycleBrand'   => new MotorcycleBrandResource($this->motorcycleModel->motorcycleBrand),
            'motorcycleModel'   => new MotorcycleModelResource($this->motorcycleModel),
            'motorcycleYear'    => new MotorcycleYearResource($motorcycleYear),
            'coverages'         => new QuotationCoverageResource($this->quotation),
            'ceroKm'            => $this->cero_km,
            'promotionCode'     => $this->promotion_code,
            'type_usage'        => $this->type_usage,
            'usage_code'        => $this->usage_code
        ];
    }
}
