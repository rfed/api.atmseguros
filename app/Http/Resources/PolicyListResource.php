<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PolicyListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // todo agregar ciudad
        return [
            "id"                => $this->id,
            "status"            => $this->status,
            "policyNumber"      => $this->policy_number,
            "proposalNumber"    => $this->proposal_number,
            "client"            => new ClientResource($this->client),
            "city"              => new CityResource($this->city),
            'province'          => new ProvinceResource($this->province),
            "postalCode"        => $this->postal_code,
            "motorcycle"        => new MotorcycleDataResource($this->motorcycleData),
            "paymentData"       => $this->paymentData,
            "quotationCoverage" => $this->quotationCoverage,
            "documents"         => PolicyDocumentResource::collection($this->documents)
        ];
    }
}
