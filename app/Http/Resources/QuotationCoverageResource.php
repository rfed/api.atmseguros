<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuotationCoverageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $coverages = $this->coverages;
        $coverages_cred = $coverages->where('payment_method', 'TARJETA');
        $array = [];
        foreach ($coverages_cred as $cov) {
            $cash = $coverages->where('payment_method', 'EFVO')->where('coverage_id', $cov->coverage_id)->first();

            $ob = [
                'id'                    => $cov->id,
                'quotationId'           => $cov->quotation_id,
                'coverageId'            => $cov->coverage_id,
                'coverageCode'          => $cov->coverage->code,
                'coverageTitle'         => $cov->coverage->title,
                'coverageDescription'   => $cov->coverage->description,
                'coverageDetails'       => $cov->coverage->details,
                'benefits'              => CoverageBenefitResource::collection($cov->coverage->coverageBenefits),
                'paymentMethod'         => $cov->payment_method,
                'feeQuantity'           => $cov->fee_quantity,
                'feeValue'              => round($cov->fee_value),
                'cashFeeValue'          => !empty($cash) ? round($cash->fee_value) : null,
                'cashPrize'             => !empty($cash) ? $cash->prize : null,
                'prize'                 => $cov->prize,
                'value'                 => $this->value,
                'plan_cot'              => $cov->plan_cot
            ];
            $array[] = $ob;
        }
        return $array;
    }
}
