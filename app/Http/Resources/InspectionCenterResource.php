<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InspectionCenterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->centro_id,
            "description"           => $this->centro_nombre,
            "agency"                => $this->centro_agencia,
            "email"                 => $this->centro_email,
            "address"               => $this->centro_direccion,
            "phone"                 => $this->centro_tel,
            "cellphone"             => $this->centro_cel,
            "schedule"              => $this->centro_horario,
            "complaintSchedule"    => $this->centro_denuncias,
            "latitude"              => $this->centro_map_lat,
            "longitude"             => $this->centro_map_lng,
            "observation"           => $this->centro_obs,
            "producer"              => $this->prodlargo,
            "cityCode"             => $this->postal_code,
            "cityDescription"      => $this->city_description,
            "provinceDescription"  => $this->prov_description,
            "provinceCode"         => $this->prov_code,
        ];
    }
}
