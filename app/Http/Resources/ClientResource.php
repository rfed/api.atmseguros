<?php

namespace App\Http\Resources;

use App\Entities\Api\CivilStatus;
use App\Entities\Api\Nacionality;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'document'      => $this->document,
            'name'          => $this->name,
            'lastName'      => $this->last_name,
            'civilStatus'   => new CivilStatusResource($this->civilStatus),
            'gender'        => $this->gender,
            'areaCode'      => $this->area_code,
            'cellphone'     => $this->cellphone,
            'birthday'      => $this->birthday,
            'nacionality'   => new NacionalityResource($this->nacionality),
            'city'          => !empty($this->city) ? new CityResource($this->city) : null,
            'addressNumber' => $this->adress_floor,
            'addressApartment' => $this->adress_apartment,
            'activyCode'    => $this->activity_id
        ];
    }
}
