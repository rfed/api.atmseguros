<?php

namespace App\Http\Resources\Responses;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->token->user;
        return [
            'id' => $this->token->user->id,
            'user' => $this->token->user->name,
            'email' => $this->token->user->email,
            'token' => $this->accessToken,
            'tokenType' => 'Bearer',
            'role' => $user->role->key,
            'roleName' => $user->role->name,
        ];
    }
}
