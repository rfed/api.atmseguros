<?php

namespace App\Http\Resources\Motorcycle;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorcycleDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'            => $this->id,
            'domain'        => $this->domain,
            'motorNumber'   => $this->motor_number,
            'motorChassis'  => $this->motor_chassis
        ];
    }
}
