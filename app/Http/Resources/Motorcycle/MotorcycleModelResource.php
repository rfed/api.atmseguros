<?php

namespace App\Http\Resources\Motorcycle;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorcycleModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'description'       => $this->description,
            'code'              => $this->code,
            'motorcycleBrandId' => $this->motorcycle_brand_id,
            'tau_codia'         => $this->infoautocod,
            'usage_code'        => $this->usage_code,
            'type_usage'        => $this->type_usage,
            'isSuggested'       => $this->is_suggested,
            'order'             => $this->order
        ];
    }
}
