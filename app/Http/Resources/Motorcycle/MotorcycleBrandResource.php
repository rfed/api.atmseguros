<?php

namespace App\Http\Resources\Motorcycle;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorcycleBrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'description'   => $this->description,
            'code'          => $this->code,
            'section'       => $this->section,
            'isSuggested'   => $this->is_suggested,
            'order'         => $this->order
        ];
    }
}
