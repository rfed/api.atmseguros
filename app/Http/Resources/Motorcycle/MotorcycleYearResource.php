<?php

namespace App\Http\Resources\Motorcycle;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorcycleYearResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'infoautocod'   => $this->infoautocod,
            'year'          => $this->year,
            'value'         => $this->value
        ]; 
    }
}
