<?php

namespace App\Http\Resources\Motorcycle;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorcycleUsageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'code'          => $this->code,
            'description'   => $this->description,
            'cotiza'        => $this->cotiza
        ];
    }
}
