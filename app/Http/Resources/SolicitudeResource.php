<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SolicitudeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'      => $this->name,
            'lastName'  => $this->last_name,
            'email'     => $this->name,
            'phone'     => $this->name,
            'coverage'  => new QuotationCoverageOneResource($this->quotationCoverage)
        ];
    }
}
