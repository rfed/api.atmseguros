<?php

namespace App\Http\Resources;

use App\Entities\Api\QuotationCoverage;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class QuotationCoverageOneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $cash = $this->cashQuotation();
        return [
                'id'                    => $this->id,
                'quotationId'           => $this->quotation_id,
                'coverageId'            => $this->coverage_id,
                'coverageCode'          => $this->coverage->code,
                'coverageTitle'         => $this->coverage->title,
                'coverageDescription'   => $this->coverage->description,
                'paymentMethod'         => $this->payment_method,
                'feeQuantity'           => $this->fee_quantity,
                'feeValue'              => $this->fee_value,
                'cashFeeValue'          => !empty($cash) ? $cash->fee_value : null,
                'cashPrize'             => !empty($cash) ? $cash->prize : null,
                'prize'                 => $this->prize,
        ];
    }
}
