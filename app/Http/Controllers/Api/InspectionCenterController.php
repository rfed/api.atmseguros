<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Core\Entities\CommonInternalResponse;
use App\Http\Resources\InspectionCenterResource;
use App\Services\InspectionCenterService;
use Illuminate\Http\Request;

class InspectionCenterController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var InspectionCenterService
     */
    protected $inspectionCenterService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = InspectionCenterResource::class;

    /**
     * InspectionCenterController constructor.
     *
     * @param InspectionCenterService $inspectionCenterService
     * @return void
     */
    public function __construct(InspectionCenterService $inspectionCenterService)
    {
        parent::__construct();
        $this->inspectionCenterService = $inspectionCenterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->respondWithCollection($this->inspectionCenterService->getFromAtm());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->errorNotImplemented();
    }
}
