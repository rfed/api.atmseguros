<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Http\Resources\ProvinceResource;
use App\Services\ProvinceService;
use Illuminate\Http\Request;

class ProvinceController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var ProvinceService
     */
    protected $provinceService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = ProvinceResource::class;

    /**
     * ProvinceController constructor.
     *
     * @param ProvinceService $provinceService
     * @return void
     */
    public function __construct(ProvinceService $provinceService)
    {
        parent::__construct();
        $this->provinceService = $provinceService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if( !empty(auth('api')->user()) && auth('api')->user()->isRole('admin')){
            return $this->respondWithCollection($this->provinceService->allWithTrashed());
        } else {
            return $this->respondWithCollection($this->provinceService->all()->sortBy('description'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->errorNotImplemented();
    }
}
