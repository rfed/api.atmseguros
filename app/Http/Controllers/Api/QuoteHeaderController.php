<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Http\Resources\QuoteHeaderResource;
use App\Services\ClientService;
use App\Services\CoverageService;
use App\Services\QuotationCoverageService;
use App\Services\QuotationService;
use App\Services\QuoteHeaderService;
use App\Services\WebService\WsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuoteHeaderController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var QuoteHeaderService
     */
    protected $quoteHeaderService;
    protected $wsService;
    protected $coverageService;
    protected $quotationService;
    protected $quotationCoverageService;
    protected $clientService;


    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = QuoteHeaderResource::class;

    /**
     * QuoteHeaderController constructor.
     *
     * @param QuoteHeaderService $quoteHeaderService
     * @param WsService $wsService
     * @param CoverageService $coverageService
     * @param QuotationService $quotationService
     * @param QuotationCoverageService $quotationCoverageService
     */
    public function __construct(QuoteHeaderService $quoteHeaderService,
                                WsService $wsService,
                                CoverageService $coverageService,
                                QuotationService $quotationService,
                                QuotationCoverageService $quotationCoverageService,
                                ClientService $clientService)
    {
        parent::__construct();
        $this->quoteHeaderService       = $quoteHeaderService;
        $this->wsService                = $wsService;
        $this->coverageService          = $coverageService;
        $this->quotationService         = $quotationService;
        $this->quotationCoverageService = $quotationCoverageService;
        $this->clientService            = $clientService;
    }

    public function newQuote(Request $request) {
        DB::beginTransaction();
        try {
            $client = $this->clientService->store($request->get('client'));
            $request->request->add(["client_id" => $client->id]);

            // store the header of the quote
            $quote_header = $this->quoteHeaderService->store($request->all());
            
            if ($quote_header) {
                // quote between webservice
                $quotation_response = $this->wsService->newQuote($quote_header);
                
                // store quotation with request and response of webservice
                $quotation = $this->quotationService->storeFromQuote($quote_header->id, $quotation_response);

                if (!$quotation_response['error']) {
                    // store coverages of the quote
                    $this->quotationCoverageService->storeFromQuote($quotation->id, $quotation_response['data'], $quote_header);
                    
                    DB::commit();
                    return $this->respondWithSuccess('', new QuoteHeaderResource($quote_header));
                    
                } else {
                    DB::commit();
                    return $this->respondWithError($quotation_response['message']);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithError($e->getMessage() . $e->getFile() . $e->getLine());
        }


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        return $this->respondWithCollection($this->quoteHeaderService->listPaginated(15, true, 'created_at', 'desc'));
    }
}
