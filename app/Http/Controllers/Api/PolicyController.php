<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Http\Resources\PolicyListResource;
use App\Http\Resources\PolicyResource;
use App\Mail\Welcome;
use App\Services\ClientService;
use App\Services\InspectionCenterService;
use App\Services\MotorcycleDataService;
use App\Services\PaymentDataService;
use App\Services\PolicyDocumentService;
use App\Services\PolicyService;
use App\Services\WebService\WsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PolicyController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var PolicyService
     */
    protected $policyService;
    protected $clientService;
    protected $motorcycleDataService;
    protected $paymentDataService;
    protected $wsService;
    protected $policyDocumentService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = PolicyResource::class;

    /**
     * PolicyController constructor.
     *
     * @param PolicyService $policyService
     * @param ClientService $clientService
     * @param MotorcycleDataService $motorcycleDataService
     * @param PaymentDataService $paymentDataService
     * @param WsService $wsService
     * @param PolicyDocumentService $policyDocumentService
     */
    public function __construct(PolicyService $policyService,
                                ClientService $clientService,
                                MotorcycleDataService $motorcycleDataService,
                                PaymentDataService $paymentDataService,
                                WsService $wsService,
                                PolicyDocumentService $policyDocumentService)
    {
        parent::__construct();
        $this->policyService = $policyService;
        $this->clientService = $clientService;
        $this->motorcycleDataService = $motorcycleDataService;
        $this->paymentDataService = $paymentDataService;
        $this->wsService = $wsService;
        $this->policyDocumentService = $policyDocumentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $this->resource = PolicyListResource::class;
        $list = $this->policyService->listPaginated(15, true, 'created_at', 'DESC', ['pay_pending' => 0]);
        return $this->respondWithCollection($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $data = $request->all();
        try {
            // store the header of the quote
            $data['client']['birthday'] = Carbon::createFromFormat('d/m/Y', $data['client']['birthday'])->format('Y-m-d');
            $data['effective_date'] = Carbon::createFromFormat('d/m/Y', $data['effective_date'])->format('Y-m-d');
            
            $client = $this->clientService->findOrUpdate($data['client']);

            $motorcycle = $this->motorcycleDataService->findOrCreate($data['motorcycle']);
            $payment = $data['payment'];
            
            if($data['payment']){
                $data['payment']['expires_in'] = Carbon::createFromFormat('m/y', $data['payment']['expires_in'])->format('Y-m-d');
                $payment = $this->paymentDataService->store($data['payment']);
            }

            $policy = $this->policyService->findOrCreate($client, $motorcycle,$payment, $data);

            if ($policy) {
                $policy_response = $this->wsService->newPolicy($policy);
                $policy = $this->policyService->updateFromPolicy($policy, $policy_response);
                DB::commit();
                $hasError = $policy_response['error'];
                $hasPay = $data['payment'];
                if( !$hasPay && !$hasError){
                    return $this->respondWithSuccess('Emitido parcial');
                }elseif (!$hasError && $hasPay ){
                    $document_response =  $this->policyDocumentService->printDocuments($policy);
                    return $this->respondWithItem($policy);
                }else{
                    return $this->respondWithError($policy_response['message']);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithError($e->getMessage() . $e->getFile() . $e->getLine());
        }

    }

    public function printFiles($policy_id)
    {
        $policy = $this->policyService->one($policy_id);
        $document_response =  $this->policyDocumentService->printDocuments($policy);
        return $this->respondWithItem($policy);
    }

    public function validateVehicle(Request $request)
    {
        $data = $request->all();
        $data['effective_date'] = Carbon::createFromFormat('d/m/Y', $data['effective_date'])->format('dmY');
        $policy_response = $this->wsService->validateVehicle($data);
        if (!$policy_response['error']) {
            return $this->respondWithSuccess($policy_response['message']);
        } else {
            return $this->respondWithError($policy_response['message']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->errorNotImplemented();
    }
}
