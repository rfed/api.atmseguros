<?php

namespace App\Http\Controllers\Api\Motorcycle;

use App\Core\Controller\BaseController;
use App\Http\Resources\Motorcycle\MotorcycleYearResource;
use App\Services\MotorcycleYearService;
use Illuminate\Http\Request;

class MotorcycleYearController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var MotorcycleYearService
     */
    protected $motorcycleYearService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = MotorcycleYearResource::class;

    /**
     * MotorcycleYearController constructor.
     *
     * @param MotorcycleYearService $motorcycleYearService
     * @return void
     */
    public function __construct(MotorcycleYearService $motorcycleYearService)
    {
        parent::__construct();
        $this->motorcycleYearService = $motorcycleYearService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->errorNotImplemented();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->errorNotImplemented();
    }
    
    public function getValueByMotorcycleModelAndYear($model_id, $year) {
        return response()->json([
            'data' => [
                'value' => $this->motorcycleYearService->getValueByMotorcycleModelAndYear($model_id, $year)
            ]
        ]);
    }
}
