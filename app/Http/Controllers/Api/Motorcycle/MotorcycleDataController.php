<?php

namespace App\Http\Controllers\Api\Motorcycle;

use App\Core\Controller\BaseController;
use App\Http\Resources\Motorcycle\MotorcycleDataResource;
use App\Services\MotorcycleDataService;
use Illuminate\Http\Request;

class MotorcycleDataController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var MotorcycleDataService
     */
    protected $motorcycleDataService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = MotorcycleDataResource::class;

    /**
     * MotorcycleDataController constructor.
     *
     * @param MotorcycleDataService $motorcycleDataService
     * @return void
     */
    public function __construct(MotorcycleDataService $motorcycleDataService)
    {
        parent::__construct();
        $this->motorcycleDataService = $motorcycleDataService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->errorNotImplemented();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->errorNotImplemented();
    }
}
