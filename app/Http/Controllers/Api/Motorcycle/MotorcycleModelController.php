<?php

namespace App\Http\Controllers\Api\Motorcycle;

use App\Core\Controller\BaseController;
use App\Http\Resources\Motorcycle\MotorcycleModelResource;
use App\Services\MotorcycleModelService;
use Illuminate\Http\Request;

class MotorcycleModelController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var MotorcycleModelService
     */
    protected $motorcycleModelService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = MotorcycleModelResource::class;

    /**
     * MotorcycleModelController constructor.
     *
     * @param MotorcycleModelService $motorcycleModelService
     * @return void
     */
    public function __construct(MotorcycleModelService $motorcycleModelService)
    {
        parent::__construct();
        $this->motorcycleModelService = $motorcycleModelService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if( !empty(auth('api')->user()) && auth('api')->user()->isRole('admin')){
            return $this->respondWithCollection($this->motorcycleModelService->allWithTrashed());
        } else {
            return $this->respondWithCollection($this->motorcycleModelService->all());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->respondWithItem($this->motorcycleModelService->one($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $object = $this->motorcycleModelService->one($id, true);
        $responseValue = $this->motorcycleModelService->update($request->all(), $object);
        if(!empty($responseValue))
            return $this->respondWithItem($responseValue);
        return $this->respondWithError("Error al actualizar el modelo");    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $object = $this->motorcycleModelService->one($id, true);
        $object->delete();
        return $this->respondWithSuccess("El modelo {$object->description} fue dado de baja correctamente.");
    }

    public function getByMotorcycleBrand($brand_id) {
        return $this->respondWithCollection($this->motorcycleModelService->getByMotorcycleBrand($brand_id));
    }

    public function getByMotorcycleBrandAndYear($brand_id, $year) {
        return $this->respondWithCollection($this->motorcycleModelService->getByMotorcycleBrandAndYear($brand_id, $year));
    }
    
}
