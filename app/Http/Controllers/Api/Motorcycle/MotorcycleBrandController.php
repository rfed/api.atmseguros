<?php

namespace App\Http\Controllers\Api\Motorcycle;

use App\Core\Controller\BaseController;
use App\Http\Resources\Motorcycle\MotorcycleBrandResource;
use App\Services\MotorcycleBrandService;
use Illuminate\Http\Request;

class MotorcycleBrandController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var MotorcycleBrandService
     */
    protected $motorcycleBrandService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = MotorcycleBrandResource::class;

    /**
     * MotorcycleBrandController constructor.
     *
     * @param MotorcycleBrandService $motorcycleBrandService
     * @return void
     */
    public function __construct(MotorcycleBrandService $motorcycleBrandService)
    {
        parent::__construct();
        $this->motorcycleBrandService = $motorcycleBrandService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if( !empty(auth('api')->user()) && auth('api')->user()->isRole('admin')){
            return $this->respondWithCollection($this->motorcycleBrandService->allWithTrashed());
        } else {
            return $this->respondWithCollection($this->motorcycleBrandService->getAll());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->respondWithItem($this->motorcycleBrandService->one($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {
        $object = $this->motorcycleBrandService->one($id, true);
        $responseValue = $this->motorcycleBrandService->update($request->all(), $object);
        if(!empty($responseValue))
            return $this->respondWithItem($responseValue);
        return $this->respondWithError("Error al actualizar la marca");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $object = $this->motorcycleBrandService->one($id, true);
        $object->delete();
        return $this->respondWithSuccess("La marca {$object->description} fue dada de baja correctamente.");
    }
}
