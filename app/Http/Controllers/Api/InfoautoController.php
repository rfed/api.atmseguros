<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Entities\Api\FtpCredential;
use App\Repositories\MotorcycleBrandRepository;
use App\Repositories\MotorcycleModelRepository;
use App\Repositories\MotorcycleYearRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InfoautoController extends BaseController
{
    private $motorcycleBrandRepository;
    private $motorcycleModelRepository;
    private $motorcycleYearRepository;


    public static $FIELDS_MARCA_MODELO = [
        'brand_code'    => 'cod_marca',
        'brand_desc'    => 'marca',
        'model_code'    => 'cod_model',
        'model_desc'    => 'modelo',
        'infoauto_code' => 'tau_codia',
        'usage_code'    => 'cod_uso',
        'usage_type'    => 'tipo_uso',
    ];

    public static $FIELDS_INFOAUTO = [
        'brand_code'    => 'tau_nmarc',
        'brand_desc'    => 'tau_marca',
        'model_code'    => 'tau_nmode',
        'model_desc'    => 'tau_model',
        'infoauto_code' => 'tau_codia',
        '-'             => 'tau_cgrup',
        'current_year'  => 'tau_anioe',
        'value_prefix'  => 'tau_pre',
        'value_length'  => 30,
    ];

    public function __construct(
        MotorcycleBrandRepository $brandRepository,
        MotorcycleModelRepository $modelRepository,
        MotorcycleYearRepository $yearRepository)
    {
        parent::__construct();
        $this->motorcycleBrandRepository = $brandRepository;
        $this->motorcycleModelRepository = $modelRepository;
        $this->motorcycleYearRepository = $yearRepository;
    }

    public function importBrandsAndModels() {
        set_time_limit(10000);
        ini_set('memory_limit', '-1');
        try {
            $marca_mod = public_path('ftp_files/wS_AU_MARCA_MODELO');
            $marca_mod_data = parseCsvToArray($marca_mod);

            $brands = 0;
            $models = 0;
            $b_updated = 0;
            $m_updated = 0;

            $infoauto_c = static::$FIELDS_MARCA_MODELO["infoauto_code"];
            $m_brand_c  = static::$FIELDS_MARCA_MODELO["brand_code"];
            $m_brand_d  = static::$FIELDS_MARCA_MODELO["brand_desc"];
            $m_model_c  = static::$FIELDS_MARCA_MODELO["model_code"];
            $m_model_d  = static::$FIELDS_MARCA_MODELO["model_desc"];
            $usage_c    = static::$FIELDS_MARCA_MODELO["usage_code"];
            $usage_type = static::$FIELDS_MARCA_MODELO["usage_type"];

            Log::debug('Importacion wS_AU_MARCA_MODELO - Comienzo: ' . date('m-d-Y H:i:s'));

            foreach ($marca_mod_data as $k => $info) {
                // todo poner seccion correcta - decidir si se pone en años o no
                $section_code = $info[$infoauto_c] >= 30000 ? 3 : 4;
                $m_bran_db = $this->motorcycleBrandRepository->findByValues(['code' => $info[$m_brand_c]]);
                if (empty($m_bran_db)) {
                    $atr = [    'code'          => $info[$m_brand_c],
                                'description'   => trim($info[$m_brand_d]),
                                'section'       => $section_code
                            ];
                    $m_bran_db = $this->motorcycleBrandRepository->store($atr);
                    $brands++;
                } else {
                    if (trim($m_bran_db->description) !== trim($info[$m_brand_d])) {
                        $m_bran_db->update([ 'description' => trim($info[$m_brand_d]) ]);
                        $b_updated++;
                    }
                }

                if( strlen($info[$usage_c]) == 3 ) {
                    $info[$usage_c] = '0'.$info[$usage_c];
                }

                // MODELS
                $m_model_db = $this->motorcycleModelRepository->findByValues(
                                    [   'infoautocod'           => $info[$infoauto_c],
                                        'motorcycle_brand_id'   => $m_bran_db->id,
                                        'usage_code'            => $info[$usage_c]
                                    ]);
                if (empty($m_model_db)) {
                    $atr = [
                        'code'                  => $info[$m_model_c],
                        'description'           => trim($info[$m_model_d]),
                        'infoautocod'           => $info[$infoauto_c],
                        'usage_code'            => $info[$usage_c],
                        'type_usage'            => $info[$usage_type],
                        'motorcycle_brand_id'   => $m_bran_db->id
                    ];

                    $m_model_db = $this->motorcycleModelRepository->store($atr);
                    $models++;
                } else {
                    if (trim($m_model_db->description) !== trim($info[$m_model_d])) {
                        $m_model_db->update([ 'description' => trim($info[$m_model_d]) ]);
                        $m_updated++;
                    }
                }
            };
            Log::debug("Importacion wS_AU_MARCA_MODELO:
                                Marcas Created: {$brands} - Marcas Updated: {$b_updated}
                                Modelos Created: {$models} - Modelos Updated: {$m_updated}
                                Fin: ". date('m-d-Y H:i:s'));
        } catch (\Exception $exception) {
            Log::error('Error wS_AU_MARCA_MODELO - ERROR: ' . $exception->getMessage());
            return $this->respondWithError($exception->getMessage());
        }
        Log::debug('Importacion wS_AU_MARCA_MODELO - Fin: ' . date('m-d-Y H:i:s'));

        return $this->respondWithSuccess('Importacion correcta');

    }

    public function importValues()
    {
        set_time_limit(10000);
        // sin limite de memoria
        ini_set('memory_limit', '-1');
        DB::disableQueryLog();

        try {
            $infoauto = public_path('ftp_files/WS_AU_INFOAUTO');
            $infoauto_data = parseCsvToArray($infoauto);
            // setting position of csv on vars
            $infoauto_code = static::$FIELDS_INFOAUTO["infoauto_code"];
            $current_year = static::$FIELDS_INFOAUTO["current_year"];
            $value_prefix = static::$FIELDS_INFOAUTO["value_prefix"];
            $value_length = static::$FIELDS_INFOAUTO["value_length"];

            Log::debug('Importacion WS_AU_INFOAUTO - Comienzo: ' . date('m-d-Y H:i:s'));
            // init variables de control
            $y_created = 0;
            $y_updated = 0;
            $in_file = count($infoauto_data);
            $years_for_insert = [];
            $years_for_update = [];
            foreach ($infoauto_data as $k => $info) {
                // Busco los años de esa marca y modelo
                // todo si quieren usan "tau_codia" cambienlo
                $years = $this->motorcycleYearRepository->getModel()->newQuery()->where([
                    'infoautocod' => $info[$infoauto_code],
                ])->get();
                for ($i = 0; $i < $value_length; $i++) {
                    $year = (int)$info[$current_year] - $i;
                    $padded_i = str_pad((string)($i + 1), 2, '0', STR_PAD_LEFT);
                    $anio_key = "{$value_prefix}{$padded_i}";
                    $value = (float)$info[$anio_key] * 1000;

                    // busco registro del año en bd
                    $m_year_db = $years->firstWhere('year', $year);
                    if (!empty($m_year_db)) {
                        if ($value == 0) {
                            // soft delete
                            $m_year_db->delete();
                            // genero cases de updates
                        } elseif (round($value, 2) != round($m_year_db->value, 2)) {
                            $case = "WHEN {$m_year_db->id} then {$value}";
                            $id = $m_year_db->id;
                            $years_for_update[] = ["case" => $case, "id" => $id];
                            $y_updated++;
                        } else {
                            continue;
                        }
                    } else {
                        // genero inserts
                        if ($value > 0) {
                            $atr = [
                                // todo si quieren usar "tau_codia" cambienlo
                                'infoautocod' => $info[$infoauto_code],
                                'year' => $year,
                                'value' => $value
                            ];
                            $years_for_insert[] = $atr;
                            $y_created++;
                        }
                    }
                }
            }
            // array chunk para dividir los inserts y updates para evitar overflow
            // inserts de años de forma masiva
            if (count($years_for_insert) > 0 ) {
                foreach (array_chunk($years_for_insert, 5000) as $inserts) {
                    DB::table('motorcycle_years')->insert($inserts);
                }
                // unset para disminuir memoria en uso
                unset($years_for_insert);
            }

            if (count($years_for_update) > 0) {
                // update masivo usando update con case https://stackoverflow.com/questions/12754470/mysql-update-case-when-then-else
                foreach (array_chunk($years_for_update, 5000) as $updates) {
                    $ids = array_column($updates, 'id');
                    $cases = array_column($updates, 'case');
                    $ids = implode(',',$ids);
                    $cases = implode(' ', $cases);
                    DB::update("UPDATE motorcycle_years
                                          SET `value` = CASE `id` {$cases} END
                                          WHERE motorcycle_years.id in ({$ids})");
                }
                // unset para disminuir memoria en uso
                unset($years_for_update);
            }
            Log::debug("Importacion WS_AU_INFOAUTO:
                                Total: {$in_file} - Anios Created: {$y_created} - Anios Updated: {$y_updated}
                                Fin: ". date('m-d-Y H:i:s'));
            return $this->respondWithSuccess('Importacion de sumas correctas');

        } catch (\Exception $exception) {
            Log::error('Error WS_AU_INFOAUTO - ERROR: ' . $exception->getMessage());
            return $this->respondWithError($exception->getMessage());
        }
    }

}
