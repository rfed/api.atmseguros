<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Core\Entities\CommonInternalResponse;
use App\Http\Resources\SolicitudeResource;
use App\Mail\HireByPhoneMail;
use App\Services\SolicitudeService;
use App\Services\TicketService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SolicitudeController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var SolicitudeService
     */
    protected $solicitudeService;

    /**
     * @var TicketService
     */
    protected $ticketService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = SolicitudeResource::class;

    /**
     * SolicitudeController constructor.
     *
     * @param SolicitudeService $solicitudeService
     * @return void
     */
    public function __construct(SolicitudeService $solicitudeService, TicketService $ticketService)
    {
        parent::__construct();
        $this->solicitudeService = $solicitudeService;
        $this->ticketService = $ticketService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->respondWithCollection($this->solicitudeService->listPaginated(15, true, 'created_at', 'desc'));
    }

    public function store(Request $request)
    {
        try {
            $resp = $this->solicitudeService->store($request->all());
            
            if (!empty($resp)) {
                Mail::to($resp->email)->send(new HireByPhoneMail($resp->name));
                $this->ticketService->generateTicket($request);
                return $this->respondWithSuccess('Solicitud guardada con exito.');
            }
        } catch (\Exception $e) {
            return $this->respondWithError('Error al guardar la solicitud, intente luego.');
        }


    }

}
