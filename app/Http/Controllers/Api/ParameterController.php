<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Http\Resources\ParameterResource;
use App\Services\ParameterService;
use Illuminate\Http\Request;

class ParameterController extends BaseController
{
    /**
     * Api Service Instance
     *
     * @var ParameterService
     */
    protected $parameterService;

    /**
     * Laravel Resource Instance
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource = ParameterResource::class;

    /**
     * ParameterController constructor.
     *
     * @param ParameterService $parameterService
     * @return void
     */
    public function __construct(ParameterService $parameterService)
    {
        parent::__construct();
        $this->parameterService = $parameterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if( !empty(auth('api')->user()) && auth('api')->user()->isRole('admin')){
            return $this->respondWithCollection($this->parameterService->allWithTrashed());
        } else {
            return $this->respondWithCollection($this->parameterService->all());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->respondWithItem($this->parameterService->one($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $object = $this->parameterService->one($id, true);
        $responseValue = $this->parameterService->update($request->all(), $object);
        if(!empty($responseValue))
            return $this->respondWithItem($responseValue);
        return $this->respondWithError("Error al actualizar el parametro");    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $object = $this->parameterService->one($id, true);
        $object->delete();
        return $this->respondWithSuccess("El parametro {$object->description} fue dado de baja correctamente.");
    }
}
