<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller\BaseController;
use App\Services\TicketService;
use Illuminate\Http\Request;

class TicketController extends BaseController
{
    private $ticketService;

    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) 
    {
        $this->ticketService->generateTicket($request->all());
        return $this->respondWithSuccess('Ticket generado');
    }
}
