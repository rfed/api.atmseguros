<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class MotorcycleUsage extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorcycle_usages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'description',
        'code',
        'cotiza'
    ];

    public function motorcycleModels() {
        return $this->hasMany(MotorcycleModel::class, 'usage_code', 'code');
    }

}
