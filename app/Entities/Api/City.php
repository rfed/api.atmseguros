<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class City extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description',
        'province_id',
        'order'
    ];

    public function province() {
        return $this->belongsTo(Province::class,  'province_id');
    }
}
