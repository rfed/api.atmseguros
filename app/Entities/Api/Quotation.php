<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Quotation extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quotations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'quotation_header_id',
        'value',
        'request',
        'response',
        'request_json',
        'response_json',
        'operation_code',
        'error',
        'message'
    ];

    public function quoteHeader() {
        return $this->belongsTo(QuoteHeader::class, 'quotation_header_id');
    }

    public function coverages() {
        return $this->hasMany(QuotationCoverage::class, 'quotation_id');
    }
    
    public function coveragesByPayment($payment) {
        return $this->coverages->where('payment_method', $payment);
    }
}
