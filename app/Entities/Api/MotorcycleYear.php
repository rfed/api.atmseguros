<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcycleYear extends BaseEntity
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorcycle_years';

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'infoautocod',
        'year',
        'value',
        'is_suggested'
    ];

    public function motorcycleModel() {
        return $this->belongsTo(MotorcycleModel::class, 'infoautocod', 'infoautocod');
    }

}
