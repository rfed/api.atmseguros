<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class PaymentData extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_datas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'method',
        'card_number',
        'code_bank',
        'bank',
        'titular_name',
//        'titular_last_name',
        'titular_document',
        'expires_in'
    ];

    public function policy() {
        return $this->hasOne(Policy::class, 'payment_data_id', 'id');
    }

}
