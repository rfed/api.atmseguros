<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Alarm extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'alarms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'description'
    ];

}
