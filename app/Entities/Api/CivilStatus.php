<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class CivilStatus extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'civil_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description'
    ];

}
