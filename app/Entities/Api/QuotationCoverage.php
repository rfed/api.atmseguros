<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;
use Illuminate\Support\Facades\DB;

class QuotationCoverage extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quotation_coverages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quotation_id',
        'coverage_id',
        'payment_method',
        'fee_quantity',
        'fee_value',
        'prize',
        'adjustment_clause',
        'extra_data',
        'plan_cot'
    ];

    public function quotation() {
        return $this->belongsTo(Quotation::class, 'quotation_id');
    }

    public function coverage() {
        return $this->belongsTo(Coverage::class, 'coverage_id');
    }

    public function cashQuotation() {
        return DB::table('quotation_coverages')
            ->select(['*'])
            ->where('quotation_id', $this->quotation_id)
            ->where('coverage_id', $this->coverage_id )
            ->where('payment_method', 'EFVO')
            ->first();
    }

}
