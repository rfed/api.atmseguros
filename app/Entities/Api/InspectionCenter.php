<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class InspectionCenter extends BaseEntity
{
    /**
     * The table associated with the model.
     * tabla de atm
     *
     * @var string
     */
    protected $table = 'centro';
    protected $connection = 'mysql_ATM';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'centro_id',
        'localidad_id',
        'centro_nombre',
        'centro_agencia',
        'centro_email',
        'centro_direccion',
        'centro_tel',
        'centro_cel',
        'centro_horario',
        'centro_denuncias',
        'centro_map_lat',
        'centro_map_lng',
        'centro_orden',
        'centro_obs',
        'prodlargo',
        'provincia_id',
        'activado',
        'postal_code',
        'city_description',
        'prov_description',
        'prov_code',
    ];

}
