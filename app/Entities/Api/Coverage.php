<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Coverage extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coverages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'title',
        'details',
        'description',
        'seccion',
        'active'
    ];


    public function coverageBenefits() {
        return $this->hasMany(CoverageBenefit::class, 'coverage_id');
    }

}
