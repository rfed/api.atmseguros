<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcycleBrand extends BaseEntity
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorcycle_brands';

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description',
        'is_suggested',
        'order',
        'section'
    ];

    public function motorcycleModels() {
        return $this->hasMany(MotorcycleModel::class, 'motorcycle_brand_id', 'id');
    }

    public function scopeAuto($query)
    {
        return $query->where('section', 3); 
    }

    public function scopeMoto($query)
    {
       return $query->where('section', 4); 
    }

    public function isAuto()
    {
        return $this->section == 3;
    }

    public function isMoto()
    {
        return $this->section == 4;
    }
}
