<?php

namespace App\Entities\Api;


use App\Core\Entities\BaseEntity;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcycleModel extends BaseEntity
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorcycle_models';

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description',
        'infoautocod',
        'usage_code',
        'type_usage',
        'is_suggested',
        'order',
        'motorcycle_brand_id'
    ];

    public function motorcycleBrand() {
        return $this->belongsTo(MotorcycleBrand::class, 'motorcycle_brand_id', 'id');
    }

    public function motorcycleYears() {
        return $this->hasMany(MotorcycleYear::class, 'infoautocod', 'infoautocod');
    }

    public function motorcycleUsage() {
        return $this->belongsTo(MotorcycleUsage::class, 'usage_code', 'code');
    }
    
    public function quoteHeaders() {
        return $this->hasMany(QuoteHeader::class, 'motorcycle_model_id', 'id');
    }
}
