<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Solicitude extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitudes';

    public $allowed_sorts = [
        'created_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'phone',
        'email',
        'quotation_coverage_id'
    ];

    public function quotationCoverage() {
        return $this->belongsTo(QuotationCoverage::class, 'quotation_coverage_id');
    }

}
