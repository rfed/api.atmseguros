<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class PolicyDocument extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'policy_documents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'url',
        'policy_id',
        'type',
        'request',
        'response',
        'request_json',
        'response_json',
        'error'
    ];

    public function policy() {
        return $this->belongsTo(Policy::class, 'policy_id');
    }

}
