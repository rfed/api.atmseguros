<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class QuoteHeader extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quotation_headers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id',
        'client_id',
        'province_id',
        'motorcycle_model_id',
        'year',
        'has_alarm',
        'alarm_id',
        'cero_km',
        'rastreo',
        'promotion_code',
        'usage_code',
        'type_usage',
        'motorcycle_value',
        'postal_code'
    ];

    public function city() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function province() {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }

    public function motorcycleModel() {
        return $this->belongsTo(MotorcycleModel::class, 'motorcycle_model_id', 'id');
    }

    public function alarm() {
        return $this->hasOne(Alarm::class, 'id','alarm_id');
    }

    public function quotation() {
        return $this->hasOne(Quotation::class, 'quotation_header_id');
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }


}
