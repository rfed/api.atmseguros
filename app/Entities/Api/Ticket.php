<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Ticket extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'data', 'description'
    ];

}
