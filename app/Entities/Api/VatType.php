<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class VatType extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vat_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'description',
        'code'
    ];

}
