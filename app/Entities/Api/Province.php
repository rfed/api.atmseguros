<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Province extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description',
        'order'
    ];

    public function cities() {
        return $this->hasMany(City::class, 'province_id');
    }

}
