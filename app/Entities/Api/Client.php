<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Client extends BaseEntity
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'document',
        'email',
        'name',
        'last_name',
        'gender',
        'area_code',
        'cellphone',
        'birthday',
        'nacionality_id',
        'address_street',
        'address_number',
        'address_floor',
        'address_apartment',
        'civil_status_id',
        'city_id',
        'postal_code',
        'province_id',
        'activity_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    public function nacionality() {
        return $this->belongsTo(Nacionality::class, 'nacionality_id');
    }

    public function civilStatus() {
        return $this->belongsTo(CivilStatus::class, 'civil_status_id');
    }

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function policy() {
        return $this->hasOne(Policy::class, 'client_id', 'id');
    }
    public function province() {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }

    public function quoteHeaders() {
        return $this->hasMany(QuoteHeader::class);
    }

}
