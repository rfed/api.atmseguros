<?php

namespace App\Entities\Api\Credentials;

use App\Core\Entities\BaseEntity;

class SystemCredential extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_credentials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
