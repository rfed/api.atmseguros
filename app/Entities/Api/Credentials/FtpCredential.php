<?php

namespace App\Entities\Api\Credentials;

use App\Core\Entities\BaseEntity;

class FtpCredential extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ftp_credentials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'user',
        'password',
        'production'
    ];

}
