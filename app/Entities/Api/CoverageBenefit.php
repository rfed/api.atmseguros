<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class CoverageBenefit extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coverage_benefits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coverage_id',
        'benefit_id',
        'has_benefit'
    ];

    public function coverage() {
        return $this->belongsTo(Coverage::class, 'coverage_id');
    }

    public function benefit() {
        return $this->belongsTo(Benefit::class, 'benefit_id');
    }

}
