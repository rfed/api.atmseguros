<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Policy extends BaseEntity
{
    public $allowed_sorts = ['created_at', 'id'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'policies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quotation_coverage_id',
        'client_id',
        'motorcycle_data_id',
        'payment_data_id',
        'inspection_center_code',
        'effective_date',
        'request',
        'response',
        'request_json',
        'response_json',
        'error',
        'policy_number',
        'proposal_number',
        'status',
        'pay_pending',
        'city_id',
        'postal_code',
        'province_id'
    ];
    public function quotationCoverage() {
        return $this->hasOne(QuotationCoverage::class, 'id', 'quotation_coverage_id');
    }

    public function client() {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function motorcycleData() {
        return $this->hasOne(MotorcycleData::class, 'id', 'motorcycle_data_id');
    }

    public function paymentData() {
        return $this->hasOne(PaymentData::class, 'id', 'payment_data_id');
    }

    public function inspectionCenter() {
        return $this->hasOne(InspectionCenter::class, 'id', 'inspection_center_id');
    }

    public function documents() {
        return $this->hasMany(PolicyDocument::class, 'policy_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id','city_id');
    }
    public function province()
    {
        return $this->hasOne(Province::class, 'id','province_id');
    }


}
