<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Benefit extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benefits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'description',
        'details'
    ];

}
