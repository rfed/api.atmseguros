<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class MotorcycleData extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorcycle_datas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'domain',
        'motor_number',
        'motor_chassis',
    ];

    public function policy() {
        return $this->hasOne(Policy::class, 'motorcycle_data_id', 'id');
    }

}
