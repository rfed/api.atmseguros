<?php

namespace App\Entities\Api;

use App\Core\Entities\BaseEntity;

class Nacionality extends BaseEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nacionalities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'description'
    ];

}
