<?php

namespace App\Entities\User;

use App\Core\Entities\BaseEntity;

class Role extends BaseEntity
{
    private static $_SYSTEM = 'system';
    private static $_SUPERADMIN = 'superadmin';
    private static $_ADMIN = 'admin';
    private static $_USER = 'user';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'key'
    ];

    public function users() {
        return $this->hasMany(User::class);
    }

    public static function getSystemKey() {
        return self::$_SYSTEM;
    }

    public static function getSuperadminKey() {
        return self::$_SUPERADMIN;
    }

    public static function getAdminKey() {
        return self::$_ADMIN;
    }

    public static function getUserKey() {
        return self::$_USER;
    }

}
