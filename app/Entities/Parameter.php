<?php

namespace App\Entities;

use App\Core\Entities\BaseEntity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parameter extends BaseEntity
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'parameters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'tag',
        'description',
        'value',
        'enabled'
    ];

    public function scopeVendedorCotizacion($query)
    {
        return $query->where('tag', '=', 'VENDEDOR_COT')->where('enabled', 1)->first();
    }

    public function scopeVendedorEmision($query)
    {
        return $query->where('tag', '=', 'VENDEDOR_EMI')->where('enabled', 1)->first();
    }
}
