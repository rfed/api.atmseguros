<?php
return [
    [
        'tag' 			=> 'CONTRATAR_ONLINE',
        'description'	=> 'Parametro que deshabilita o habilita el boton contratar online en el sitio',
        'enabled'       => 1
    ],
    [
        'tag' 			=> 'CONTRATAR_TELEFONO',
        'description'	=> 'Parametro que deshabilita o habilita el boton contratar telefono en el sitio',
        'enabled'       => 1
    ],
    [
        'tag' 			=> 'MERCADOPAGO',
        'description'	=> 'Parametro que deshabilita o habilita pagar con mercadopago en el sitio',
        'enabled'       => 0
    ],
    [
        'tag' 			=> 'TODOPAGO',
        'description'	=> 'Parametro que deshabilita o habilita pagar con todopago en el sitio',
        'enabled'       => 0
    ],
    [
        'tag' 			=> 'MAX_ANIO_VIGENTE',
        'description'	=> 'Maximo anio vigente que se mostrara en sitio',
        'enabled'       => 1,
        'value'         => "2019"
    ],

];