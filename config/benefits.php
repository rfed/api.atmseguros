<?php
return [
    ['desc' => 'Responsabilidad Civil $10.000.000', 'details' => ''],
    ['desc' => 'Auxilio Y Remolque', 'details' => ''],
    ['desc' => 'MotoDescuentos', 'details' => ''],
    ['desc' => 'Robo / Hurto Total', 'details' => ''],
    ['desc' => 'Incendio Total', 'details' => ''],
    ['desc' => 'Destrucción Total Por Accidente', 'details' => ''],
    ['desc' => 'Sin Franquicias (1)', 'details' => 'La cobertura de robo clásico está alcanzada por una franquicia del 3%'],
    ['desc' => 'Reposición De Moto Robada (2)', 'details' => 'Reposición de la misma moto durante el primer año. Válido para motos 0km o con certificado de no rodamiento. A partir de sumas aseguradas mayores a $40.000.'],
    ['desc' => 'Ajuste Automático De Suma Asegurada (3)', 'details' => 'Ajuste automático hasta un 30% de la suma asegurada.'],
];