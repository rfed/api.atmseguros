<?php
return [
    [
        'code' 			=> 'A0',
        'benefits'      => [
            [
                'desc' => 'Responsabilidad Civil $10.000.000',
                'active' => true,
            ],
            [
                'desc' => 'Auxilio Y Remolque',
                'active' => true,
            ],
            [
                'desc' => 'MotoDescuentos',
                'active' => true,
            ],
            [
                'desc' => 'Robo / Hurto Total',
                'active' => false,
            ],
            [
                'desc' => 'Incendio Total',
                'active' => false,
            ],
            [
                'desc' => 'Destrucción Total Por Accidente',
                'active' => false,
            ],
            [
                'desc' => 'Protección Ante Granizo',
                'active' => false,
            ],
            [
                'desc' => 'Sin Franquicias (1)',
                'active' => false,
            ],
            [
                'desc' => 'Reposición De Moto Robada (2)',
                'active' => false,
            ],
            [
                'desc' => 'Ajuste Automático De Suma Asegurada (3)',
                'active' => false,
            ],
        ],
    ],
    [
        'code' 			=> 'B0',
        'benefits'      => [
            [
                'desc' => 'Responsabilidad Civil $10.000.000',
                'active' => true,
            ],
            [
                'desc' => 'Auxilio Y Remolque',
                'active' => true,
            ],
            [
                'desc' => 'MotoDescuentos',
                'active' => true,
            ],
            [
                'desc' => 'Robo / Hurto Total',
                'active' => true,
            ],
            [
                'desc' => 'Incendio Total',
                'active' => true,
            ],
            [
                'desc' => 'Destrucción Total Por Accidente',
                'active' => true,
            ],
            [
                'desc' => 'Protección Ante Granizo',
                'active' => true,
            ],
            [
                'desc' => 'Sin Franquicias (1)',
                'active' => true,
            ],
            [
                'desc' => 'Reposición De Moto Robada (2)',
                'active' => true,
            ],
            [
                'desc' => 'Ajuste Automático De Suma Asegurada (3)',
                'active' => true,
            ],
        ]
    ],
    [
        'code' 			=> 'B1',
        'benefits'      => [
            [
                'desc' => 'Responsabilidad Civil $10.000.000',
                'active' => true,
            ],
            [
                'desc' => 'Auxilio Y Remolque',
                'active' => true,
            ],
            [
                'desc' => 'MotoDescuentos',
                'active' => true,
            ],
            [
                'desc' => 'Robo / Hurto Total',
                'active' => true,
            ],
            [
                'desc' => 'Incendio Total',
                'active' => true,
            ],
            [
                'desc' => 'Destrucción Total Por Accidente',
                'active' => false,
            ],
            [
                'desc' => 'Protección Ante Granizo',
                'active' => false,
            ],
            [
                'desc' => 'Sin Franquicias (1)',
                'active' => false,
            ],
            [
                'desc' => 'Reposición De Moto Robada (2)',
                'active' => false,
            ],
            [
                'desc' => 'Ajuste Automático De Suma Asegurada (3)',
                'active' => false,
            ],
        ]
    ],

];