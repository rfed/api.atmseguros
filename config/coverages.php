<?php
return [
    [
        'code' 			=> 'A0',
        'title'			=> 'Responsabilidad civil',
        'description'	=> 'RESPONSABILIDAD CIVIL SOLAMENTE',
        'details'       => 'Asegurá tu moto frente a daños materiales, lesiones a terceros y/o transportados.'
    ],
    [
        'code' 			=> 'B0',
        'title'			=> 'Robo Premium',
        'description'	=> 'PREMIUM - ROBO y/o HURTO TOTAL,INC.TOTAL Y PERDIDA TOT.POR ACCIDENTE',
        'details'       => 'Estás por contratar la cobertura más
                            completa del mercado, que incluye
                            protección frente a granizo, reposición
                            de moto robada y ¡12 servicios de
                            AUXILIO Y REMOLQUE GRATUITOS!'
    ],
    [
        'code' 			=> 'B1',
        'title'			=> 'Robo Clasico',
        'description'	=> 'ROBO y/o HURTO TOTAL, INCENDIO TOTAL',
        'details'       => 'Protegé tu moto con nuestro seguro contra robo, hurto e incendio total.'
    ],

];