<?php

return [
    'ADMINISTRATOR' => [
        'code' => '001',
        'description' => 'Administrador',
        'sub_modules' => [
            'USER' => [
                'code' => '01',
                'description' => 'Usuarios',
                'sub_modules' => [
                    'USER_VIEW' => [
                        'code' => '01',
                        'description' => 'Listar'
                    ],
                    'USER_ADD' => [
                        'code' => '02',
                        'description' => 'Agregar'
                    ],
                    'USER_UPDATE' => [
                        'code' => '03',
                        'description' => 'Editar'
                    ],
                    'USER_DELETE' => [
                        'code' => '04',
                        'description' => 'Eliminar'
                    ],
                ]
            ],
            'PROFILE' => [
                'code' => '02',
                'description' => 'Perfiles',
                'sub_modules' => [
                    'PROFILE_VIEW' => [
                        'code' => '01',
                        'description' => 'Listar'
                    ],
                    'PROFILE_ADD' => [
                        'code' => '02',
                        'description' => 'Agregar'
                    ],
                    'PROFILE_UPDATE' => [
                        'code' => '03',
                        'description' => 'Editar'
                    ],
                    'PROFILE_DELETE' => [
                        'code' => '04',
                        'description' => 'Eliminar'
                    ],
                ]
            ],
            'SUPER_MODULE' => [
                'code' => '03',
                'description' => 'Modulos',
                'sub_modules' => [
                    'SUPER_MODULE_VIEW' => [
                        'code' => '01',
                        'description' => 'Listar'
                    ],
                    'SUPER_MODULE_ADD' => [
                        'code' => '02',
                        'description' => 'Agregar'
                    ],
                ],
            ],
        ],
    ],
];
