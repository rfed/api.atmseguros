## INSTALACIÓN 

#### INSTALAR PROYECTO (corre todos los comandos para la instalacion del proyecto)
    php artisan app:install
#### REINSTALAR PROYECTO (corre todos los comandos para la instalacion del proyecto, hace un fresh para la DB)
    php artisan app:install 
        (sin opcion) -> migrate
        -r -> migrate:fresh        
        
        
## PARA DESARROLLO

### Crear Clases
    php artisan make:entity Namespace/Example -a
    
    php artisan make:api-controller Namespace/ExampleController --model="Namespace/Example"
    
    php artisan make:repository Namespace/Example -i 
    
    php artisan make:service Namespace/Example -i
    
    php artisan make:resource Namespace/ExampleResource
    
    php artisan make:request Namespace/ExampleStoreRequest
        
    php artisan make:request Namespace/ExampleUpdateRequest
    
    php artisan make:policy Namespace/ExamplePolicy
    
    php artisan make:seeder ExampleSeeder
    
### Crear notificacion CUSTOM

    php artisan make:notification ExampleActionNotification --markdown=mail.example.action        
