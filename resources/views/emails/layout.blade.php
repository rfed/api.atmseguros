<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="date=no" />
    <meta name="format-detection" content="address=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Fira+Mono:400,500,700|Ubuntu:400,400i,500,500i,700,700i" rel="stylesheet" />
    <!--<![endif]-->
    <title>ATM Seguros</title>
    <!--[if gte mso 9]>
    <style type="text/css" media="all">
        sup { font-size: 100% !important; }
    </style>
    <![endif]-->


    <style type="text/css" media="screen">
        /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#2e57ae; -webkit-text-size-adjust:none }
        a { color:#000001; text-decoration:none }
        p { padding:0 !important; margin:0 !important }
        img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }

        /* Mobile styles */
        @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
            .mobile-shell { width: 100% !important; min-width: 100% !important; }

            .m-center { text-align: center !important; }
            .text-footer,
            .text-header { text-align: center !important; }


            .td { width: 100% !important; min-width: 100% !important; }

            .p30-15 { padding: 30px 15px !important; }
            .p30-15-0 { padding: 30px 15px 0px 15px !important;
            .container { padding: 0px !important; }
            .separator { padding-top: 30px !important; }

            .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

            .column,
            .column-empty { padding-bottom: 10px !important; }

        }
    </style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#808080; -webkit-text-size-adjust:none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#808080">
    <tr>
        <td align="center" valign="top" class="container" style="padding:50px 10px;">
            <!-- Container -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                            <tr>
                                <td class="td" bgcolor="#ffffff" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tr>
                                            <td class="p30-15-0" style="padding: 20px 20px 0px 20px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                        <a href="https://www.atmseguros.com.ar/" target="_blank">
                                                                            <img src="{{ asset('images/logo-atm.png') }}"  height="45" border="0" alt="" />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="column-empty" width="1"></th>
                                                        <th class="column" width="120">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="text-header right" style="text-align:right;">
                                                                        <img src="{{ asset('images/metric-logo.png') }}"  height="35" border="0" alt="" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="separator" style="padding-top: 20px; border-bottom:4px solid #8E133E; font-size:0pt; line-height:0pt;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END Header -->

                                    <!-- Intro -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tr>
                                            <td  style="padding: 10px 30px 10px 50px; color:#000000; font-family:'Ubuntu', Arial,sans-serif; font-size:15px; line-height:25px; text-align:center;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    @yield('content')
                                                </table>
                                            </td>
                                        </tr>

                                    </table>
                                    <!-- END Intro -->


                                    <!-- Footer -->
                                    {{--<table width="100%" border="0" cellspacing="0" cellpadding="0">--}}
                                        {{--<tr>--}}
                                            {{--<td class="footer" style="padding: 0px 30px 30px 30px;">--}}
                                                {{--<table width="100%" border="0" cellspacing="0" cellpadding="0">--}}
                                                    {{--<tr>--}}
                                                        {{--<td class="separator" style="border-bottom:4px solid #000000; font-size:0pt; line-height:0pt;"></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<td class="pb40" style="padding-bottom:40px;"></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<td class="text-socials" style="color:#000000; font-family:'Fira Mono', Arial,sans-serif; font-size:16px; line-height:22px; text-align:center; text-transform:uppercase;">follow us</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<td style="padding: 30px 0px 30px 0px;" align="center">--}}
                                                            {{--<table class="center" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">--}}
                                                                {{--<tr>--}}
                                                                    {{--<td class="img" width="52" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t_free_ico_facebook.jpg" width="42" height="42" border="0" alt="" /></a></td>--}}
                                                                    {{--<td class="img" width="52" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t_free_ico_twitter.jpg" width="42" height="42" border="0" alt="" /></a></td>--}}
                                                                    {{--<td class="img" width="52" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t_free_ico_gplus.jpg" width="42" height="42" border="0" alt="" /></a></td>--}}
                                                                    {{--<td class="img" width="42" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t_free_ico_instagram.jpg" width="42" height="42" border="0" alt="" /></a></td>--}}
                                                                {{--</tr>--}}
                                                            {{--</table>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                {{--</table>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                    {{--</table>--}}
                                    {{--<!-- END Footer -->--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            <tr>
                             <td>

                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000">
                                     <tr>
                                         <td class="p30-15-0" style="padding: 20px 20px 20px 20px;">
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                     <th class="column">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                             <tr>
                                                                 <td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                     <a href="https://www.atmseguros.com.ar/" target="_blank">
                                                                         <img src="{{ asset('images/logo-atm.png') }}"  height="45" border="0" alt="" />
                                                                     </a>
                                                                 </td>
                                                             </tr>
                                                         </table>
                                                     </th>
                                                     <th class="column" width="500">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                             <tr>
                                                                 <td class="text-header right" style="text-align:right; color: white;  font-family: 'Source Code Pro' ,sans-serif; font-size:10px;">
                                                                     www.ssn.gob.ar | 0800.666.8400 | Nro de Inscripción SSN: 0812
                                                                 </td>
                                                             </tr>
                                                         </table>
                                                     </th>
                                                 </tr>
                                             </table>
                                         </td>
                                     </tr>
                                 </table>
                             </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END Container -->
        </td>
    </tr>
</table>
</body>
</html>
