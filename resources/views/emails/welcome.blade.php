@extends('emails.layout')

@section('content')
    <h2 style="color: #6AC33A">¡Felicitaciones!</h2>
    <p style="text-align: left">
        Hola, <b>{{ $name }}</b>
        <br>
        Ya estás asegurado en la empresa de
        seguros más confiable del país. <br>
        Bienvenido al mundo de ATM SEGUROS, <br>
    </p>
    <p> Te adjuntamos los documentos de tu {{ $type === 'POL' ? 'Póliza' : 'Propuesta' }}</p>
    <p>        ¡Saludo motero!
    </p>
@endsection