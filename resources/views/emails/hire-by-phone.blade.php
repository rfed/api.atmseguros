@extends('emails.layout')

@section('content')
    <h2>Contratación por teléfono</h2>
    <p style="text-align: left">
        Hola, <b>{{ $name }}</b>
        <br>
        Su solicitud ya ha sido registrada, espere hasta que se comuniquen con usted
    </p>
    <p> Saludos</p>

@endsection