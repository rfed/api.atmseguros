@extends('emails.layout')

@section('content')

    Cotizacion erronea.<br>
    Nombre: {{ $client->last_name.' '.$client->name }}<br>
    Email: {{ $client->email }}<br>
    Celular: {{ $client->area_code.'-'.$client->cellphone }}<br>
    ------------------------------------<br>
    Seccion: {{ $brand->isAuto() ? "AUTO" : "MOTO" }}<br>
    Marca: {{ $brand->description }}<br>
    Modelo: {{ $model->description }}<br>
    Valor: {{  $quoteHeader->motorcycle_value }}<br>
    Año: {{ $quoteHeader->year }} <br>
    Localidad: {{ $localidad }} <br>
    Provincia: {{ $client->province->description }}<br>
    ------------------------------------<br>
    ID Marca: {{ $brand->id }} <br>
    ID Modelo: {{ $model->id }}<br>
    ID CODIGO POSTAL: {{ $postal_code }}<br>
    Error:  <br>
    @foreach ($message as $msg)
        - {{ $msg }}<br>
    @endforeach
    <hr>
    Enviado el {{ date('d/m/Y') }} <br> 
    ------------------------------------
    <br>

@endsection
